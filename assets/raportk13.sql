-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 04, 2020 at 08:02 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `raportk13`
--

-- --------------------------------------------------------

--
-- Table structure for table `m_ekstrakurikuler`
--

CREATE TABLE `m_ekstrakurikuler` (
  `id_eks` int(11) NOT NULL,
  `nama_eks` varchar(50) NOT NULL,
  `kkm_eks` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `m_ekstrakurikuler`
--

INSERT INTO `m_ekstrakurikuler` (`id_eks`, `nama_eks`, `kkm_eks`) VALUES
(1, 'Futsal', 75),
(2, 'Renang', 75),
(3, 'Marawis & Hadroh', 75);

-- --------------------------------------------------------

--
-- Table structure for table `m_guru`
--

CREATE TABLE `m_guru` (
  `id_guru` int(11) NOT NULL,
  `nip` varchar(25) DEFAULT '-',
  `nama_guru` varchar(50) NOT NULL,
  `email` varchar(25) DEFAULT '-',
  `no_hp` varchar(13) NOT NULL,
  `alamat` varchar(255) DEFAULT '-',
  `gambar` varchar(255) NOT NULL DEFAULT 'default.png'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `m_guru`
--

INSERT INTO `m_guru` (`id_guru`, `nip`, `nama_guru`, `email`, `no_hp`, `alamat`, `gambar`) VALUES
(1, 'NIP001', 'Fajri', 'fajri@mail.com', '012876821', '<p>Cikarang</p>', 'default.png');

-- --------------------------------------------------------

--
-- Table structure for table `m_mapel`
--

CREATE TABLE `m_mapel` (
  `id_mapel` int(11) NOT NULL,
  `nama_mapel` varchar(75) NOT NULL,
  `kkm_mapel` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `m_mapel`
--

INSERT INTO `m_mapel` (`id_mapel`, `nama_mapel`, `kkm_mapel`) VALUES
(1, 'Bahasa Indonesia', 75),
(2, 'Matematika', 73),
(3, 'Bahasa Inggris', 73);

-- --------------------------------------------------------

--
-- Table structure for table `m_mulok`
--

CREATE TABLE `m_mulok` (
  `id_mulok` int(11) NOT NULL,
  `mulok` varchar(75) NOT NULL,
  `kkm` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `m_mulok`
--

INSERT INTO `m_mulok` (`id_mulok`, `mulok`, `kkm`) VALUES
(1, 'Bahasa Sunda', 75),
(2, 'Al-Qur\'an dan Hadits', 75);

-- --------------------------------------------------------

--
-- Table structure for table `m_siswa`
--

CREATE TABLE `m_siswa` (
  `id_siswa` int(11) NOT NULL,
  `nis` varchar(50) NOT NULL,
  `nisn` varchar(50) NOT NULL,
  `nama_siswa` varchar(100) NOT NULL,
  `jk` varchar(15) NOT NULL,
  `tmp_lahir` varchar(25) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `anakke` varchar(2) DEFAULT NULL,
  `alamat` text DEFAULT NULL,
  `asal_sek` varchar(50) DEFAULT NULL,
  `ortu_ayah` varchar(75) NOT NULL,
  `ortu_ibu` varchar(75) NOT NULL,
  `ortu_alamat` text DEFAULT NULL,
  `ortu_telp` varchar(13) DEFAULT NULL,
  `ortu_email` varchar(50) NOT NULL,
  `foto_siswa` varchar(255) NOT NULL DEFAULT 'default.png',
  `dibuat` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `m_siswa`
--

INSERT INTO `m_siswa` (`id_siswa`, `nis`, `nisn`, `nama_siswa`, `jk`, `tmp_lahir`, `tgl_lahir`, `anakke`, `alamat`, `asal_sek`, `ortu_ayah`, `ortu_ibu`, `ortu_alamat`, `ortu_telp`, `ortu_email`, `foto_siswa`, `dibuat`) VALUES
(1, '90923478', '662328238', 'Ardiansyah Runtuboy', 'Laki-laki', 'Bekasi Timur', '2005-09-13', '2', 'Cikarang Barat Bekasi', 'MTsn 1 Bekasi', 'Rudiansyah', 'Ida Nurhayati', 'Sleman', '01231297', 'rudi@mail.com', 'default.png', '2020-09-27 04:41:00');

-- --------------------------------------------------------

--
-- Table structure for table `m_tahun_ajaran`
--

CREATE TABLE `m_tahun_ajaran` (
  `id_ta` int(11) NOT NULL,
  `tahun_ajaran` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `m_tahun_ajaran`
--

INSERT INTO `m_tahun_ajaran` (`id_ta`, `tahun_ajaran`) VALUES
(1, '2020/2021'),
(2, '2021/2022');

-- --------------------------------------------------------

--
-- Table structure for table `m_user`
--

CREATE TABLE `m_user` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `aktif` int(11) NOT NULL,
  `akses` int(11) NOT NULL,
  `avatar` varchar(255) NOT NULL DEFAULT 'default.png',
  `dibuat` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `m_user`
--

INSERT INTO `m_user` (`id`, `nama`, `username`, `password`, `aktif`, `akses`, `avatar`, `dibuat`) VALUES
(1, 'Administrator', 'admin', '$2y$10$ERORjHwi/fs4IbQx6c8ZEOheYVvgwv3Mh4mTCHQmz9fvQAA2n29TG', 1, 1, 'default.png', '2020-09-22 14:06:48'),
(2, 'Muhamad Arul Sendri', 'arulsen', '$2y$10$8Y.6WOhQSPBfUmZJft2KseKtoLvCuFUR/TCcgK7JqFxHuRLRXu5Cq', 1, 2, 'default.png', '2020-09-22 07:07:54'),
(3, 'Fajri', 'fajri', '$2y$10$NmlUcHwuMKxg4ZnQOkGUUegcp17qyXGXWrMzijPidVGidsJh3pJ6m', 1, 3, 'default.png', '2020-09-22 07:08:49');

-- --------------------------------------------------------

--
-- Table structure for table `m_user_akses`
--

CREATE TABLE `m_user_akses` (
  `id_akses` int(11) NOT NULL,
  `akses` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `m_user_akses`
--

INSERT INTO `m_user_akses` (`id_akses`, `akses`) VALUES
(1, 'Adminstrator'),
(2, 'Staff'),
(3, 'Walikelas');

-- --------------------------------------------------------

--
-- Table structure for table `t_kelas`
--

CREATE TABLE `t_kelas` (
  `id_kelas` int(11) NOT NULL,
  `nama_kelas` varchar(50) NOT NULL,
  `tahun_ajaran` int(11) DEFAULT NULL,
  `wali_kelas` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `t_kelas`
--

INSERT INTO `t_kelas` (`id_kelas`, `nama_kelas`, `tahun_ajaran`, `wali_kelas`) VALUES
(1, 'X Multimedia 1', 1, 1),
(2, 'X Multimedia 2', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `t_kelas_detail`
--

CREATE TABLE `t_kelas_detail` (
  `id_kelas_detail` int(11) NOT NULL,
  `kelas` int(11) DEFAULT NULL,
  `siswa` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `m_ekstrakurikuler`
--
ALTER TABLE `m_ekstrakurikuler`
  ADD PRIMARY KEY (`id_eks`);

--
-- Indexes for table `m_guru`
--
ALTER TABLE `m_guru`
  ADD PRIMARY KEY (`id_guru`);

--
-- Indexes for table `m_mapel`
--
ALTER TABLE `m_mapel`
  ADD PRIMARY KEY (`id_mapel`);

--
-- Indexes for table `m_mulok`
--
ALTER TABLE `m_mulok`
  ADD PRIMARY KEY (`id_mulok`);

--
-- Indexes for table `m_siswa`
--
ALTER TABLE `m_siswa`
  ADD PRIMARY KEY (`id_siswa`);

--
-- Indexes for table `m_tahun_ajaran`
--
ALTER TABLE `m_tahun_ajaran`
  ADD PRIMARY KEY (`id_ta`);

--
-- Indexes for table `m_user`
--
ALTER TABLE `m_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `m_user_akses`
--
ALTER TABLE `m_user_akses`
  ADD PRIMARY KEY (`id_akses`);

--
-- Indexes for table `t_kelas`
--
ALTER TABLE `t_kelas`
  ADD PRIMARY KEY (`id_kelas`);

--
-- Indexes for table `t_kelas_detail`
--
ALTER TABLE `t_kelas_detail`
  ADD PRIMARY KEY (`id_kelas_detail`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `m_ekstrakurikuler`
--
ALTER TABLE `m_ekstrakurikuler`
  MODIFY `id_eks` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `m_guru`
--
ALTER TABLE `m_guru`
  MODIFY `id_guru` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `m_mapel`
--
ALTER TABLE `m_mapel`
  MODIFY `id_mapel` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `m_mulok`
--
ALTER TABLE `m_mulok`
  MODIFY `id_mulok` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `m_siswa`
--
ALTER TABLE `m_siswa`
  MODIFY `id_siswa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `m_tahun_ajaran`
--
ALTER TABLE `m_tahun_ajaran`
  MODIFY `id_ta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `m_user`
--
ALTER TABLE `m_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `m_user_akses`
--
ALTER TABLE `m_user_akses`
  MODIFY `id_akses` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `t_kelas`
--
ALTER TABLE `t_kelas`
  MODIFY `id_kelas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `t_kelas_detail`
--
ALTER TABLE `t_kelas_detail`
  MODIFY `id_kelas_detail` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
