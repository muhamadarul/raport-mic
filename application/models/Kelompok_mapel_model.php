<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Kelompok_mapel_model extends CI_Model
{
    private $_table = "m_kelmapel";

    public function view()
    {
        $query = $this->db->get('m_kelmapel')->result_array();
        return $query;
    }

    public function getById($id)
    {
        $query = $this->db->escape($this->db->get_where('m_kelmapel', array('id_kelmapel' => $id)));
        return $query->row_array();
    }
    public function tambah()
    {
      $post = $this->input->post();
      $this->id_kelmapel = "";
      $this->nama_kelmapel = $post["nama_kelmapel"];
      $this->db->insert($this->_table, $this);
    }

    public function ubah()
    {
          $post = $this->input->post();
          $this->id_kelmapel = $post["id_kelmapel"];
          $this->nama_kelmapel = $post["nama_kelmapel"];
          $this->db->update($this->_table, $this, array('id_kelmapel' => $post["id_kelmapel"]));
    }

    public function hapus($id)
    {
        return $this->db->delete($this->_table, array("id_kelmapel" => $id));
    }

}
