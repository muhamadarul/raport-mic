<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Login_model extends CI_Model
{

    public function login($username, $password)
    {
        $user = $this->db->get_where('m_user', ['username' => $username])->row_array();
        return $user;
    }

    public function success_login()
    {
        $user = $this->db->get_where('m_user', ['username' => $this->session->userdata('username')])->row_array();
        return $user;
    }
}
