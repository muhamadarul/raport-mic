<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Tahun_ajaran_model extends CI_Model
{
    private $_table = "m_tahun_ajaran";

    public function view()
    {
        $query = $this->db->get('m_tahun_ajaran')->result_array();
        return $query;
    }

    public function getById($id)
    {
        $query = $this->db->escape($this->db->get_where('m_tahun_ajaran', array('id_ta' => $id)));
        return $query->row_array();
    }
    public function tambah()
    {
      $post = $this->input->post();
      $this->id_ta = "";
      $this->thn_ajaran = $post["tahun_ajaran"];
      $this->db->insert($this->_table, $this);
    }

    public function ubah()
    {
          $post = $this->input->post();
          $this->id_ta = $post["id_ta"];
          $this->thn_ajaran = $post["tahun_ajaran"];
          $this->db->update($this->_table, $this, array('id_ta' => $post["id_ta"]));
    }

    public function hapus($id)
    {
        return $this->db->delete($this->_table, array("id_ta" => $id));
    }

}
