<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Siswa_model extends CI_Model
{
    private $_table = "m_siswa";
    public $foto_siswa = "default.png";

    public function view()
    {
        $query = $this->db->get('m_siswa')->result_array();
        return $query;
    }
    public function getSiswa($siswa,$column)
    {
      $this->db->select('*');
      $this->db->limit(10);
      $this->db->from('m_siswa');
      $this->db->like('nama_siswa', $siswa);
      return $this->db->get()->result_array();
    }
  
    public function getById($id)
    {
        $query = $this->db->escape($this->db->get_where('m_siswa', array('id_siswa' => $id)));
        return $query->row_array();
    }
    public function tambah()
    {
      $post = $this->input->post();
      $this->id_siswa = "";
      $this->nis = $post["nis"];
      $this->nisn = $post["nisn"];
      $this->nama_siswa = $post["nama_siswa"];
      $this->jk = $post["jk"];
      $this->tmp_lahir = $post["tmp_lahir"];
      $this->tgl_lahir = $post["tgl_lahir"];
      $this->anakke = $post["anakke"];
      $this->alamat= $post["alamat"];
      $this->asal_sek = $post["asal_sek"];
      $this->ortu_ayah = $post["ortu_ayah"];
      $this->ortu_ibu = $post["ortu_ibu"];
      $this->ortu_alamat= $post["ortu_alamat"];
      $this->ortu_telp= $post["ortu_telp"];
      $this->ortu_email= $post["ortu_email"];
      $this->foto_siswa = $this->_uploadImage();
      $this->db->insert($this->_table, $this);
    }

    public function ubah()
    {
          $post = $this->input->post();
          $this->id_siswa = $post["id_siswa"];
          $this->nis = $post["nis"];
          $this->nisn = $post["nisn"];
          $this->nama_siswa = $post["nama_siswa"];
          $this->jk = $post["jk"];
          $this->tmp_lahir = $post["tmp_lahir"];
          $this->tgl_lahir = $post["tgl_lahir"];
          $this->anakke = $post["anakke"];
          $this->alamat= $post["alamat"];
          $this->asal_sek = $post["asal_sek"];
          $this->ortu_ayah = $post["ortu_ayah"];
          $this->ortu_ibu = $post["ortu_ibu"];
          $this->ortu_alamat= $post["ortu_alamat"];
          $this->ortu_telp= $post["ortu_telp"];
          $this->ortu_email= $post["ortu_email"];
          if (!empty($_FILES["foto_siswa"]["name"])) {
              $this->_deleteImage($this->id_siswa);
              $this->foto_siswa = $this->_uploadImage();
          } else {
              $this->foto_siswa = $post["foto_siswa_lama"];
          }
          $this->db->update($this->_table, $this, array('id_siswa' => $post["id_siswa"]));
    }

    public function hapus($id)
    {
        $this->_deleteImage($id);
        return $this->db->delete($this->_table, array("id_siswa" => $id));
    }

    private function _uploadImage()
    {
      $config['upload_path']          = './assets/images/siswa';
      $config['allowed_types']        = 'gif|jpg|png';
      $config['file_name']            = $this->id_siswa;
      $config['overwrite']            = true;
      $config['max_size']             = 1024; // 1MB
      // $config['max_width']            = 1024;
      // $config['max_height']           = 768;

      $this->load->library('upload', $config);

      if ($this->upload->do_upload('foto_siswa')) {
          return $this->upload->data("file_name");
          // $gambar = $_FILES['userfile']['name'];
      }
      return "default.png";
  }

  private function _deleteImage($id)
  {
      $query = $this->db->get_where($this->_table,["id_siswa" => $id])->row();
      if ($query->foto_siswa != "default.png") {
          $filename = explode(".", $query->foto_siswa)[0];
          return array_map('unlink', glob(FCPATH . "assets/images/siswa/$filename.*"));
        }
  }

  function select()
  {
      $this->db->order_by('id_siswa', 'DESC');
      $query = $this->db->get('m_siswa');
      return $query;

  }

  function import($data)
  {
    $this->db->insert_batch('m_siswa', $data);
  }

}
