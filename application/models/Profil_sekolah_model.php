<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Profil_sekolah_model extends CI_Model
{
    private $_table = "m_profil_sekolah";
    public $logo = "default.png";

    public function view()
    {
        $query = $this->db->escape($this->db->get_where('m_profil_sekolah', array('id' => 1)));
        return $query->row_array();
    }
    public function ubah()
    {
          $post = $this->input->post();
          $this->id = $post["id"];
          $this->nama_sekolah = $post["nama_sekolah"];
          $this->visi = $post["visi"];
          $this->misi = $post["misi"];
          $this->kepsek = $post["kepsek"];
          $this->email = $post["email"];
          $this->telpon = $post["telpon"];
          $this->web = $post["web"];
          $this->alamat= $post["alamat"];
          if (!empty($_FILES["logo"]["name"])) {
              $this->_deleteImage($this->id);
              $this->logo = $this->_uploadImage();
          } else {
              $this->logo = $post["logo_lama"];
          }
          $this->db->update($this->_table, $this, array('id' => $post["id"]));
    }


    private function _uploadImage()
    {
      $config['upload_path']          = './assets/images/logo';
      $config['allowed_types']        = 'gif|jpg|png';
      $config['file_name']            = $this->id;
      $config['overwrite']            = true;
      $config['max_size']             = 1024; // 1MB
      // $config['max_width']            = 1024;
      // $config['max_height']           = 768;

      $this->load->library('upload', $config);

      if ($this->upload->do_upload('logo')) {
          return $this->upload->data("file_name");
          // $gambar = $_FILES['userfile']['name'];
      }
      return "default.png";
  }

  private function _deleteImage($id)
  {
      $query = $this->db->get_where($this->_table,["id" => $id])->row();
      if ($query->logo != "default.png") {
          $filename = explode(".", $query->logo)[0];
          return array_map('unlink', glob(FCPATH . "assets/images/logo/$filename.*"));
        }
  }

}
