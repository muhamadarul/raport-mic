<?php

defined('BASEPATH') or exit('No direct script access allowed');

class User_model extends CI_Model
{
    private $_table = "m_user";
    public $id;
    public $avatar = "default.png";


    public function view()
    {
        $user = $this->db->get('m_user')->result_array();
        return $user;
    }
    public function viewAkses()
    {
        $akses = $this->db->get('m_user_akses')->result_array();
        return $akses;
    }
    public function getById($id)
    {
        $query = $this->db->escape($this->db->get_where('m_user', array('id' => $id)));
        return $query->row_array();
    }


    public function tambah()
    {
        $this->id = "";
        $data = [

            'nama' => htmlspecialchars($this->input->post('nama', true)),
            'username' => htmlspecialchars($this->input->post('username', true)),
            'password' => password_hash($this->input->post('password1'), PASSWORD_DEFAULT),
            'aktif' => htmlspecialchars($this->input->post('status', true)),
            'akses' => htmlspecialchars($this->input->post('akses', true)),
            'avatar' => $this->_uploadImage(),
         ];

        return $this->db->insert('m_user', $data);

    }

    public function ubah()
    {
          $post = $this->input->post();
          $this->id = $post["id"];
          $this->nama = $post["nama"];
          $this->username = $post["username"];
          $this->aktif = $post["status"];
          $this->akses = $post["akses"];
          $this->avatar = $this->_uploadImage();
          if (!empty($_FILES["avatar"]["name"])) {
              $this->_deleteImage($this->id);
              $this->avatar = $this->_uploadImage();
          } else {
              $this->avatar = $post["avatar_lama"];
          }
    $this->db->update($this->_table, $this, array('id' => $post["id"]));
    // $this->db->where('id', $this->input->post('id'));
    // $this->db->update('user', $data) ;
    }

    private function _uploadImage()
    {
      $config['upload_path']          = './assets/images/user';
      $config['allowed_types']        = 'gif|jpg|png';
      $config['file_name']            = $this->id;
      $config['overwrite']            = true;
      $config['max_size']             = 1024; // 1MB
      // $config['max_width']            = 1024;
      // $config['max_height']           = 768;

      $this->load->library('upload', $config);

      if ($this->upload->do_upload('avatar')) {
          return $this->upload->data("file_name");
          // $gambar = $_FILES['userfile']['name'];
      }
      return "default.png";
  }

  public function hapus($id)
  {
      $this->_deleteImage($id);
      return $this->db->delete($this->_table, array("id" => $id));
  }

  private function _deleteImage($id)
  {
      $admin = $this->db->get_where($this->_table,["id" => $id])->row();
      if ($admin->avatar != "default.png") {
          $filename = explode(".", $admin->avatar)[0];
          return array_map('unlink', glob(FCPATH . "assets/images/user/$filename.*"));
        }
  }
}
