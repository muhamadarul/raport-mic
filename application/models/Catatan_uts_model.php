<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Catatan_uts_model extends CI_Model
{
    private $_table = "t_catatan_uts";

    public function __construct()
     {
         parent::__construct();
         $this->load->database();
     }

    public function view()
    {
      $this->db->select('*');
      $this->db->from('t_catatan_uts');
      $this->db->join('m_tahun_ajaran', 't_catatan_uts.ta = m_tahun_ajaran.id_ta');
      $this->db->join('t_kelas', 't_kelas.id_kelas = t_catatan_uts.kelas');
      $query = $this->db->escape($this->db->get());
      return $query->result_array();
    }

    public function checkExist($ta,$kelas,$semester)
    {
        return $this->db->get_where('t_catatan_uts',array('ta'=>$ta,'kelas'=>$kelas,'semester'=>$semester))->num_rows();
    }
    public function getLastId()
    {
      $this->db->select('id_catatan_uts');
      $this->db->from('t_catatan_uts');
      $this->db->order_by('id_catatan_uts','desc');
      $query = $this->db->escape($this->db->get());
      return $query->row_array();
    }
    public function SaveNilaiDetail($data)
    {
        return $this->db->insert_batch('t_catatan_uts_detail', $data);
    }

    public function tambah($data)
    {
      return $this->db->insert('t_catatan_uts',$data);
    }
    public function viewNilaiById($id)
    {
      $this->db->select('*');
      $this->db->from('t_catatan_uts_detail');
      $this->db->join('t_catatan_uts_detail', 't_catatan_uts.id_catatan_uts = t_catatan_uts_detail.catatan_uts');
      $this->db->join('t_kelas', 't_kelas.id_kelas = t_catatan_uts.kelas');
      $this->db->join('m_tahun_ajaran', 'm_tahun_ajaran.id_ta = t_catatan_uts.ta');
      $this->db->join('m_siswa', 'm_siswa.id_siswa = t_catatan_uts_detail.siswa');
      $this->db->where('catatan_uts',$id);
      $query = $this->db->escape($this->db->get());
      return $query->result_array();
    }
    public function viewKelas($id)
    {
      $this->db->select('*');
      $this->db->from('t_catatan_uts');
      $this->db->join('t_kelas', 't_kelas.id_kelas = t_catatan_uts.kelas');
      $this->db->join('m_guru', 'm_guru.id_guru = t_kelas.wali_kelas');
      $this->db->join('m_tahun_ajaran', 'm_tahun_ajaran.id_ta = t_kelas.tahun_ajaran');
      $this->db->where('id_catatan_uts',$id);
      $query = $this->db->escape($this->db->get());
      return $query->row_array();
    }
    public function hapus($id)
    {
         $this->db->delete($this->_table, array("id_catatan_uts" => $id));
         $this->db->delete('t_catatan_uts_detail', array("catatan_uts" => $id));
    }

}
