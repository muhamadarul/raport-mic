<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Jadwal_model extends CI_Model
{
    private $_table = "t_jadwal";

    public function view()
    {
        $query = $this->db->get('t_jadwal')->result_array();
        return $query;
    }
    public function viewAll()
    {
      $this->db->select('*');
      $this->db->from('t_jadwal');
      $this->db->join('t_kelas', 't_kelas.id_kelas = t_jadwal.kelas');
      $this->db->join('m_tahun_ajaran', 'm_tahun_ajaran.id_ta = t_kelas.tahun_ajaran');
      $query = $this->db->escape($this->db->get());
      return $query->result_array();
    }
    public function viewHari()
    {
        $query = $this->db->get('m_hari')->result_array();
        return $query;
    }
    public function getIdDetail($id)
    {
      $query = $this->db->escape($this->db->get_where('t_jadwal_detail', array('id_jadwal_detail' => $id)));
      return $query->row_array();
    }
    public function checkExist($kelas)
    {
      return $this->db->get_where('t_jadwal',['kelas'=>$kelas])->num_rows();
    }
    public function detailcheckExist($itemMapel,$jadwal)
    {
      return $this->db->get_where('t_jadwal_detail',array('mapel'=>$itemMapel,'jadwal'=>$jadwal))->num_rows();
    }
    public function ambilid($id)
    {
      $this->db->select('*');
      $this->db->from('t_kelas');
      $this->db->join('m_tahun_ajaran', 'm_tahun_ajaran.id_ta = t_kelas.tahun_ajaran');
      $this->db->join('m_guru', 't_kelas.wali_kelas = m_guru.id_guru');
      $this->db->where('id_kelas',$id);
      $query = $this->db->escape($this->db->get());
      return $query->row_array();
    }
    public function getById($id)
    {
      $this->db->select('*');
      $this->db->from('t_jadwal');
      $this->db->join('t_jadwal_detail', 't_jadwal_detail.jadwal = t_jadwal.id_jadwal');
      $this->db->join('t_kelas', 't_kelas.id_kelas = t_jadwal.kelas');
      $this->db->join('m_tahun_ajaran', 'm_tahun_ajaran.id_ta = t_kelas.tahun_ajaran');
      $this->db->join('m_mapel', 't_jadwal_detail.mapel = m_mapel.id_mapel');
      $this->db->join('m_hari', 'm_hari.id_hari = t_jadwal_detail.hari');
      $this->db->join('m_guru', 'm_guru.id_guru = t_jadwal_detail.pengampu');
      $this->db->where('id_jadwal',$id);
      $query = $this->db->escape($this->db->get());
      return $query->row_array();
    }
    public function tambah($data)
    {
      $this->db->insert($this->_table, $data);
    }
    public function getLastJadwal()
    {
      $this->db->select('id_jadwal');
      $this->db->from('t_jadwal');
      $this->db->order_by('id_jadwal','desc');
      $query = $this->db->escape($this->db->get());
      return $query->row_array();
    }
    public function ubah($where,$data)
    {
        $this->db->where($where);
        $this->db->update($this->_table,$data);
    }
    public function ubahJadwalDetail($where,$data)
    {
        $this->db->where($where);
        $this->db->update('t_jadwal_detail',$data);
    }

    public function hapus($id)
    {
        $this->db->delete($this->_table, array("id_jadwal" => $id));
        $this->db->delete('t_jadwal_detail', array("jadwal" => $id));
    }

    public function JadwalDetail($id)
    {
        $this->db->select('id_jadwal_detail,nama_kelas,nama_hari,jam,nama_mapel,nama_guru,thn_ajaran');
        $this->db->from('t_jadwal');
        $this->db->join('t_jadwal_detail', 't_jadwal.id_jadwal = t_jadwal_detail.jadwal');
        $this->db->join('m_guru', 't_jadwal_detail.pengampu = m_guru.id_guru');
        $this->db->join('t_kelas', 't_jadwal.kelas = t_kelas.id_kelas');
        $this->db->join('m_mapel', 'm_mapel.id_mapel = t_jadwal_detail.mapel');
        $this->db->join('m_hari', 'm_hari.id_hari = t_jadwal_detail.hari');
        $this->db->join('m_tahun_ajaran', 'm_tahun_ajaran.id_ta = t_kelas.tahun_ajaran');
        $this->db->where('id_jadwal',$id);
        $this->db->order_by('hari','ASC');
        $query = $this->db->escape($this->db->get());
        return $query->result_array();
    }

    public function tambahJadwalDetail($data)
    {
      $this->db->insert('t_jadwal_detail', $data);
    }
    public function hapusJadwalDetail($id)
    {
      return $this->db->delete('t_jadwal_detail', array("id_jadwal_detail" => $id));
    }

}
