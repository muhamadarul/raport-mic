<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Mulok_model extends CI_Model
{
    private $_table = "m_mulok";

    public function view()
    {
        $query = $this->db->get('m_mulok')->result_array();
        return $query;
    }

    public function getById($id)
    {
        $query = $this->db->escape($this->db->get_where('m_mulok', array('id_mulok' => $id)));
        return $query->row_array();
    }
    public function tambah()
    {
      $post = $this->input->post();
      $this->id_mulok = "";
      $this->mulok = $post["mulok"];
      $this->kkm = $post["kkm"];
      $this->db->insert($this->_table, $this);
    }

    public function ubah()
    {
          $post = $this->input->post();
          $this->id_mulok = $post["id_mulok"];
          $this->mulok = $post["mulok"];
          $this->kkm = $post["kkm"];
          $this->db->update($this->_table, $this, array('id_mulok' => $post["id_mulok"]));
    }

    public function hapus($id)
    {
        return $this->db->delete($this->_table, array("id_mulok" => $id));
    }

}
