<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Beranda_model extends CI_Model
{
  public function hitunguser()
  {
      $query = $this->db->get('m_user');
      if ($query->num_rows() > 0) {
          return $query->num_rows();
      } else {
          return 0;
      }
  }
  public function hitungkelas()
  {
      $query = $this->db->get('t_kelas');
      if ($query->num_rows() > 0) {
          return $query->num_rows();
      } else {
          return 0;
      }
  }
  public function hitungmapel()
  {
      $query = $this->db->get('m_mapel');
      if ($query->num_rows() > 0) {
          return $query->num_rows();
      } else {
          return 0;
      }
  }
  public function hitungsiswa()
  {
      $query = $this->db->get('m_siswa');
      if ($query->num_rows() > 0) {
          return $query->num_rows();
      } else {
          return 0;
      }
  }
  public function hitungguru()
  {
      $query = $this->db->get('m_guru');
      if ($query->num_rows() > 0) {
          return $query->num_rows();
      } else {
          return 0;
      }
  }
  public function hitungeks()
  {
      $query = $this->db->get('m_ekstrakurikuler');
      if ($query->num_rows() > 0) {
          return $query->num_rows();
      } else {
          return 0;
      }
  }
}
