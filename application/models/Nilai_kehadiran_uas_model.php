<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Nilai_kehadiran_uas_model extends CI_Model
{
    private $_table = "t_nilai_kehadiran_uas";


    public function __construct()
     {
         parent::__construct();
         $this->load->database();
     }

    public function view()
    {
      $this->db->select('*');
      $this->db->from('t_nilai_kehadiran_uas');
      $this->db->join('m_tahun_ajaran', 't_nilai_kehadiran_uas.ta = m_tahun_ajaran.id_ta');
      $this->db->join('t_kelas', 't_kelas.id_kelas = t_nilai_kehadiran_uas.kelas');
      $query = $this->db->escape($this->db->get());
      return $query->result_array();
    }

    public function checkExist($ta,$kelas,$semester)
    {
        return $this->db->get_where('t_nilai_kehadiran_uas',array('ta'=>$ta,'kelas'=>$kelas,'semester'=>$semester))->num_rows();
    }
    public function getLastId()
    {
      $this->db->select('id_kehadiran_uas');
      $this->db->from('t_nilai_kehadiran_uas');
      $this->db->order_by('id_kehadiran_uas','desc');
      $query = $this->db->escape($this->db->get());
      return $query->row_array();
    }
    public function SaveNilaiDetail($data)
    {
        return $this->db->insert_batch('t_nilai_kehadiran_uas_detail', $data);
    }

    public function tambah($data)
    {
      return $this->db->insert('t_nilai_kehadiran_uas',$data);
    }
    public function viewNilaiById($id)
    {
      $this->db->select('*');
      $this->db->from('t_nilai_kehadiran_uas_detail');
      $this->db->join('t_nilai_kehadiran_uas', 't_nilai_kehadiran_uas.id_kehadiran_uas = t_nilai_kehadiran_uas_detail.nilai_kehadiran_uas');
      $this->db->join('t_kelas', 't_kelas.id_kelas = t_nilai_kehadiran_uas.kelas');
      $this->db->join('m_tahun_ajaran', 'm_tahun_ajaran.id_ta = t_nilai_kehadiran_uas.ta');
      $this->db->join('m_siswa', 'm_siswa.id_siswa = t_nilai_kehadiran_uas_detail.siswa');
      $this->db->where('nilai_kehadiran_uas',$id);
      $query = $this->db->escape($this->db->get());
      return $query->result_array();
    }
    public function viewKelas($id)
    {
      $this->db->select('*');
      $this->db->from('t_nilai_kehadiran_uas');
      $this->db->join('t_kelas', 't_kelas.id_kelas = t_nilai_kehadiran_uas.kelas');
      $this->db->join('m_guru', 'm_guru.id_guru = t_kelas.wali_kelas');
      $this->db->join('m_tahun_ajaran', 'm_tahun_ajaran.id_ta = t_kelas.tahun_ajaran');
      $this->db->where('id_kehadiran_uas',$id);
      $query = $this->db->escape($this->db->get());
      return $query->row_array();
    }
    public function hapus($id)
    {
         $this->db->delete($this->_table, array("id_kehadiran_uas" => $id));
         $this->db->delete('t_nilai_kehadiran_uas_detail', array("nilai_kehadiran_uas" => $id));
    }

}
