<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Guru_model extends CI_Model
{
    private $_table = "m_guru";
    public $gambar = "default.png";

    public function view()
    {
        $query = $this->db->get('m_guru')->result_array();
        return $query;
    }

    public function getById($id)
    {
        $query = $this->db->escape($this->db->get_where('m_guru', array('id_guru' => $id)));
        return $query->row_array();
    }
    public function tambah()
    {
      $post = $this->input->post();
      $this->id_guru = "";
      $this->nip = $post["nip"];
      $this->nama_guru = $post["nama_guru"];
      $this->email = $post["email"];
      $this->no_hp = $post["no_hp"];
      $this->alamat = $post["alamat"];
      $this->gambar = $this->_uploadImage();
      $this->db->insert($this->_table, $this);
    }

    public function ubah()
    {
          $post = $this->input->post();
          $this->id_guru = $post["id_guru"];
          $this->nip = $post["nip"];
          $this->nama_guru = $post["nama_guru"];
          $this->email = $post["email"];
          $this->no_hp = $post["no_hp"];
          $this->alamat = $post["alamat"];
          if (!empty($_FILES["gambar"]["name"])) {
              $this->_deleteImage($this->id_guru);
              $this->gambar = $this->_uploadImage();
          } else {
              $this->gambar = $post["gambar_lama"];
          }
          $this->db->update($this->_table, $this, array('id_guru' => $post["id_guru"]));
    }
    private function _uploadImage()
    {
      $config['upload_path']          = './assets/images/guru';
      $config['allowed_types']        = 'gif|jpg|png';
      $config['file_name']            = $this->id_guru;
      $config['overwrite']            = true;
      $config['max_size']             = 1024; // 1MB
      // $config['max_width']            = 1024;
      // $config['max_height']           = 768;

      $this->load->library('upload', $config);

      if ($this->upload->do_upload('gambar')) {
          return $this->upload->data("file_name");
          // $gambar = $_FILES['userfile']['name'];
      }
      return "default.png";
    }

    public function hapus($id)
    {
      $this->_deleteImage($id);
      return $this->db->delete($this->_table, array("id_guru" => $id));
    }

    private function _deleteImage($id)
    {
        $query = $this->db->get_where($this->_table,["id_guru" => $id])->row();
        if ($query->gambar != "default.png") {
            $filename = explode(".", $query->gambar)[0];
            return array_map('unlink', glob(FCPATH . "assets/images/guru/$filename.*"));
          }
    }

}
