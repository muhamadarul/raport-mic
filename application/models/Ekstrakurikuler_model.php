<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Ekstrakurikuler_model extends CI_Model
{
    private $_table = "m_ekstrakurikuler";

    public function view()
    {
        $query = $this->db->get('m_ekstrakurikuler')->result_array();
        return $query;
    }

    public function getById($id)
    {
        $query = $this->db->escape($this->db->get_where('m_ekstrakurikuler', array('id_eks' => $id)));
        return $query->row_array();
    }
    public function tambah()
    {
      $post = $this->input->post();
      $this->id_eks = "";
      $this->nama_eks = $post["nama_eks"];
      $this->kkm_eks = $post["kkm_eks"];
      $this->db->insert($this->_table, $this);
    }

    public function ubah()
    {
          $post = $this->input->post();
          $this->id_eks = $post["id_eks"];
          $this->nama_eks = $post["nama_eks"];
          $this->kkm_eks = $post["kkm_eks"];
          $this->db->update($this->_table, $this, array('id_eks' => $post["id_eks"]));
    }

    public function hapus($id)
    {
        return $this->db->delete($this->_table, array("id_eks" => $id));
    }

}
