<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Kelas_model extends CI_Model
{
    private $_table = "t_kelas";

    public function viewAll()
    {
      $this->db->select('*');
      $this->db->from('t_kelas');
      $this->db->join('m_tahun_ajaran', 'm_tahun_ajaran.id_ta = t_kelas.tahun_ajaran');
      $this->db->join('m_guru', 't_kelas.wali_kelas = m_guru.id_guru');
      $query = $this->db->escape($this->db->get());
      return $query->result_array();
    }
    public function getSelectedKelasTa($id)
    {
      $this->db->select('*');
      $this->db->from('t_kelas');
      $this->db->join('m_tahun_ajaran', 'm_tahun_ajaran.id_ta = t_kelas.tahun_ajaran');
      $this->db->where('tahun_ajaran',$id);
      $query = $this->db->escape($this->db->get());
      return $query->result_array();
    }
    public function viewKelasTa()
    {
      $this->db->select('*');
      $this->db->from('t_kelas');
      $this->db->join('m_tahun_ajaran', 'm_tahun_ajaran.id_ta = t_kelas.tahun_ajaran');
      $query = $this->db->escape($this->db->get());
      return $query->result_array();
    }
    public function checkExistSiswa($kelas,$itemSiswa)
    {
      return $this->db->get_where('t_kelas_detail',array('kelas'=>$kelas,'siswa'=>$itemSiswa))->num_rows();
    }
    public function view()
    {
        $query = $this->db->get('t_kelas')->result_array();
        return $query;
    }
    public function ambilid($id)
    {
      $this->db->select('*');
      $this->db->from('t_kelas');
      $this->db->join('m_tahun_ajaran', 'm_tahun_ajaran.id_ta = t_kelas.tahun_ajaran');
      $this->db->join('m_guru', 't_kelas.wali_kelas = m_guru.id_guru');
      $this->db->where('id_kelas',$id);
      $query = $this->db->escape($this->db->get());
      return $query->row_array();
    }
    public function getById($id)
    {
      $this->db->select('*');
      $this->db->from('t_kelas');
      $this->db->join('m_tahun_ajaran', 'm_tahun_ajaran.id_ta = t_kelas.tahun_ajaran');
      $this->db->join('m_guru', 't_kelas.wali_kelas = m_guru.id_guru');
      $this->db->join('t_kelas_detail', 't_kelas.id_kelas = t_kelas_detail.kelas');
      $this->db->join('m_siswa', 't_kelas_detail.siswa = m_siswa.id_siswa');
      $this->db->where('id_kelas',$id);
      $query = $this->db->escape($this->db->get());
      return $query->row_array();
    }
    public function tambah($data)
    {
      $this->db->insert($this->_table, $data);
    }
    public function getSiswaByKelas($kelas)
    {
      $this->db->select('siswa');
      $this->db->from('t_kelas_detail');
      $this->db->join('t_kelas', 't_kelas.id_kelas = t_kelas_detail.kelas');
      $this->db->where('kelas',$kelas);
      $query = $this->db->escape($this->db->get());
      return $query->result_array();
    }
    public function getLastKelas()
    {
      $this->db->select('id_kelas');
      $this->db->from('t_kelas');
      $this->db->order_by('id_kelas','desc');
      $query = $this->db->escape($this->db->get());
      return $query->row_array();
    }
    public function ubah($where,$data)
    {
        $this->db->where($where);
        $this->db->update($this->_table,$data);
    }

    public function hapus($id)
    {
         $this->db->delete($this->_table, array("id_kelas" => $id));
         $this->db->delete('t_kelas_detail', array("kelas" => $id));
    }

    public function SiswaKelas($id)
    {
        $this->db->select('id_kelas_detail,nama_siswa,nama_kelas,nis');
        $this->db->from('t_kelas');
        $this->db->join('t_kelas_detail', 't_kelas.id_kelas = t_kelas_detail.kelas');
        $this->db->join('m_siswa', 't_kelas_detail.siswa = m_siswa.id_siswa');
        $this->db->where('kelas',$id);
        $this->db->order_by('nama_siswa','ASC');
        $query = $this->db->escape($this->db->get());
        return $query->result_array();
    }
    public function detailSiswaKelas($id)
    {
      $this->db->select('*');
      $this->db->from('t_kelas');
      $this->db->join('t_kelas_detail', 't_kelas.id_kelas = t_kelas_detail.kelas');
      $this->db->join('m_siswa', 't_kelas_detail.siswa = m_siswa.id_siswa');
      $this->db->where('kelas',$id);
      $this->db->order_by('nama_siswa','ASC');
      $query = $this->db->escape($this->db->get());
      return $query->result_array();
    }
    public function tambahSiswaKelas($data)
    {
      $this->db->insert('t_kelas_detail', $data);
    }
    public function hapusSiswaKelas($id)
    {
      return $this->db->delete('t_kelas_detail', array("id_kelas_detail" => $id));
    }

}
