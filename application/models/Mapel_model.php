<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Mapel_model extends CI_Model
{
    private $_table = "m_mapel";

    public function view()
    {
        $query = $this->db->get('m_mapel')->result_array();
        return $query;
    }
    public function getMapel($mapel,$column)
    {
      $this->db->select('*');
      $this->db->limit(10);
      $this->db->from('m_mapel');
      $this->db->like('nama_mapel', $mapel);
      return $this->db->get()->result_array();
    }
    public function getSelectedMapel($id)
    {
      $this->db->select('*');
      $this->db->from('m_mapel');
      $this->db->join('t_jadwal_detail', 't_jadwal_detail.mapel = m_mapel.id_mapel');
      $this->db->where('id_mapel',$id);
      $query = $this->db->escape($this->db->get());
      return $query->result_array();
    }
    public function viewAll()
    {
      $this->db->select('*');
      $this->db->from('m_mapel');
      $this->db->join('m_kelmapel', 'm_kelmapel.id_kelmapel = m_mapel.kelmapel');
      $query = $this->db->escape($this->db->get());
      return $query->result_array();
    }

    public function getById($id)
    {
        $query = $this->db->escape($this->db->get_where('m_mapel', array('id_mapel' => $id)));
        return $query->row_array();
    }
    public function tambah()
    {
      $post = $this->input->post();
      $this->id_mapel = "";
      $this->nama_mapel = $post["nama_mapel"];
      $this->kelmapel = $post["kelmapel"];
      $this->kkm_mapel = $post["kkm_mapel"];
      $this->db->insert($this->_table, $this);
    }

    public function ubah()
    {
          $post = $this->input->post();
          $this->id_mapel = $post["id_mapel"];
          $this->nama_mapel = $post["nama_mapel"];
          $this->kelmapel = $post["kelmapel"];
          $this->kkm_mapel = $post["kkm_mapel"];
          $this->db->update($this->_table, $this, array('id_mapel' => $post["id_mapel"]));
    }

    public function hapus($id)
    {
        return $this->db->delete($this->_table, array("id_mapel" => $id));
    }

}
