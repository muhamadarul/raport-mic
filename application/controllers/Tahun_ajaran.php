<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Tahun_ajaran extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Login_model');
        $this->load->model('Tahun_ajaran_model');
        $this->load->helper('url');
        if (!$this->session->userdata('username')) {
                $this->session->set_flashdata('error', 'Anda belum melakukan login!');
                redirect('login');
        }
        if ($this->session->userdata('akses') == 3) {
                redirect('blank');
        }
    }

    public function index()
    {
        $data['user'] = $this->Login_model->success_login();
        $data['title'] = 'Tahun Ajaran';
        $data['view'] = $this->Tahun_ajaran_model->view();
        // echo 'Selamat datang ' . $data['user']['nama'];
        $this->load->view('templates/header', $data);
        $this->load->view('admin/tahun_ajaran/index', $data);
        $this->load->view('templates/footer');
    }

    public function tambah()
    {
      $data['user'] = $this->Login_model->success_login();
      $data['title'] = 'Tambah Tahun Ajaran ';

      $this->form_validation->set_rules('tahun_ajaran', 'Tahun ajaran', 'required|trim', [
          'required' => 'Tahun ajaran tidak boleh kosong!'
      ]);
      if ($this->form_validation->run() == false) {
          $this->load->view('templates/header', $data);
          $this->load->view('admin/tahun_ajaran/tambah', $data);
          $this->load->view('templates/footer');
      } else {
          $this->Tahun_ajaran_model->tambah();
          $this->session->set_flashdata('success', 'Data berhasil ditambahkan!');
          redirect('tahun_ajaran');
      }
    }

    public function hapus($id)
    {
      if ($this->session->userdata('akses') == 2) {
              redirect('blank');
      }
        $this->Tahun_ajaran_model->hapus($id);
        $this->session->set_flashdata('success', 'Data berhasil dihapus');
        redirect('tahun_ajaran');
    }

    public function ubah($id = null)
    {
        $data['user'] = $this->Login_model->success_login();
        $data['view'] = $this->Tahun_ajaran_model->getById($id);
        $data['title'] = 'Ubah Tahun Ajaran';

        $this->form_validation->set_rules('tahun_ajaran', 'Tahun ajaran', 'required|trim', [
            'required' => 'Tahun ajaran tidak boleh kosong!'
        ]);

        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data);
            $this->load->view('admin/tahun_ajaran/ubah', $data);
            $this->load->view('templates/footer');
        } else {
            $this->Tahun_ajaran_model->ubah();
            $this->session->set_flashdata('success', 'Data berhasil diubah!');
            redirect('tahun_ajaran');
        }
    }

}
