<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Siswa extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Login_model');
        $this->load->model('Siswa_model');
        $this->load->model('Profil_sekolah_model');

        $this->load->library('excel');
        if (!$this->session->userdata('username')) {
                $this->session->set_flashdata('error', 'Anda belum melakukan login!');
                redirect('login');
        }
        if ($this->session->userdata('akses') == 3) {
                redirect('blank');
        }
    }

    public function index()
    {
        $data['user'] = $this->Login_model->success_login();
        $data['title'] = 'Data Siswa';
        $data['view'] = $this->Siswa_model->view();
        // echo 'Selamat datang ' . $data['user']['nama'];
        $this->load->view('templates/header', $data);
        $this->load->view('admin/siswa/index', $data);
        $this->load->view('templates/footer');
    }

    public function tambah()
    {
      $data['user'] = $this->Login_model->success_login();
      $data['title'] = 'Tambah Siswa';
      $this->form_validation->set_rules('nis', 'NIS', 'required|trim|is_unique[m_siswa.nis]', [
          'required' => 'NIS tidak boleh kosong!',
          'is_unique' => 'NIS sudah digunakan!'
      ]);
      $this->form_validation->set_rules('nama_siswa', 'Nama', 'required|trim', [
          'required' => 'Nama Siswa tidak boleh kosong!'
      ]);
      $this->form_validation->set_rules('nisn', 'NISN', 'required|trim', [
          'required' => 'NISN tidak boleh kosong!'
      ]);
      $this->form_validation->set_rules('tmp_lahir', 'Tempat Lahir', 'required|trim', [
          'required' => 'Tempat Lahir tidak boleh kosong!'
      ]);
      $this->form_validation->set_rules('tgl_lahir', 'Tgl Lahir', 'required|trim', [
          'required' => 'Tgl Lahir tidak boleh kosong!'
      ]);
      $this->form_validation->set_rules('asal_sek', 'Asal Sekolah', 'required|trim', [
          'required' => 'Asal Sekolah tidak boleh kosong!'
      ]);
      $this->form_validation->set_rules('ortu_ayah', 'Nama Ayah', 'required|trim', [
          'required' => 'Nama Ayah tidak boleh kosong!'
      ]);
      $this->form_validation->set_rules('ortu_ibu', 'Nama Ibu', 'required|trim', [
          'required' => 'Nama Ibu tidak boleh kosong!'
      ]);

      $this->form_validation->set_rules('ortu_alamat', 'Alamat Ortu', 'required|trim', [
          'required' => 'Alamat Ortu tidak boleh kosong!'
      ]);
      $this->form_validation->set_rules('ortu_telp', 'Telepon Ortu', 'required|trim', [
          'required' => 'Telepon Ortu tidak boleh kosong!'
      ]);
      $this->form_validation->set_rules('ortu_email', 'Email Ortu', 'required|trim', [
          'required' => 'Email Ortu tidak boleh kosong!'
      ]);
      if ($this->form_validation->run() == false) {
          $this->load->view('templates/header', $data);
          $this->load->view('admin/siswa/tambah', $data);
          $this->load->view('templates/footer');
      } else {
          $this->Siswa_model->tambah();
          $this->session->set_flashdata('success', 'Data berhasil ditambahkan!');
          redirect('siswa');
      }
    }

    public function hapus($id)
    {
      if ($this->session->userdata('akses') == 2) {
              redirect('blank');
      }
        $this->Siswa_model->hapus($id);
        $this->session->set_flashdata('success', 'Data berhasil dihapus');
        redirect('siswa');
    }

    public function ubah($id = null)
    {
        $data['user'] = $this->Login_model->success_login();
        $data['view'] = $this->Siswa_model->getById($id);
        $data['title'] = 'Ubah Siswa';
        $this->form_validation->set_rules('nama_siswa', 'Nama', 'required|trim', [
            'required' => 'Nama Siswa tidak boleh kosong!'
        ]);
        $this->form_validation->set_rules('nisn', 'NISN', 'required|trim', [
            'required' => 'NISN tidak boleh kosong!'
        ]);
        $this->form_validation->set_rules('tmp_lahir', 'Tempat Lahir', 'required|trim', [
            'required' => 'Tempat Lahir tidak boleh kosong!'
        ]);
        $this->form_validation->set_rules('tgl_lahir', 'Tgl Lahir', 'required|trim', [
            'required' => 'Tgl Lahir tidak boleh kosong!'
        ]);
        $this->form_validation->set_rules('asal_sek', 'Asal Sekolah', 'required|trim', [
            'required' => 'Asal Sekolah tidak boleh kosong!'
        ]);
        $this->form_validation->set_rules('ortu_ayah', 'Nama Ayah', 'required|trim', [
            'required' => 'Nama Ayah tidak boleh kosong!'
        ]);
        $this->form_validation->set_rules('ortu_ibu', 'Nama Ibu', 'required|trim', [
            'required' => 'Nama Ibu tidak boleh kosong!'
        ]);

        $this->form_validation->set_rules('ortu_alamat', 'Alamat Ortu', 'required|trim', [
            'required' => 'Alamat Ortu tidak boleh kosong!'
        ]);
        $this->form_validation->set_rules('ortu_telp', 'Telepon Ortu', 'required|trim', [
            'required' => 'Telepon Ortu tidak boleh kosong!'
        ]);
        $this->form_validation->set_rules('ortu_email', 'Email Ortu', 'required|trim', [
            'required' => 'Email Ortu tidak boleh kosong!'
        ]);


        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data);
            $this->load->view('admin/siswa/ubah', $data);
            $this->load->view('templates/footer');
        } else {
            $this->Siswa_model->ubah();
            $this->session->set_flashdata('success', 'Data berhasil diubah!');
            redirect('siswa');
        }
    }

    public function detail($id = null)
    {

        $data['user'] = $this->Login_model->success_login();
        $data['view'] = $this->Siswa_model->getById($id);
        $data['title'] = 'Admin | Detail Admin';
            $this->load->view('templates/header', $data);
            $this->load->view('admin/siswa/detail', $data);
            $this->load->view('templates/footer');
    }

    function import_siswa()
    {
      $data['user'] = $this->Login_model->success_login();
      $data['title'] = 'Import Data Siswa';
      $this->load->view('templates/header',$data);
      $this->load->view('admin/siswa/import');
      $this->load->view('templates/footer');

    }

    function fetch(){
      $data = $this->Siswa_model->select();
      $output = '
      <h3 align="center">Total Data - '.$data->num_rows().'</h3>
      <table class="table table-striped table-bordered">
       <tr>
       <th>NIS</th>
       <th>NISN</th>
       <th>Nama</th>
       <th>Jenis Kelamin</th>
       <th>Tempat Lahir</th>
       <th>Tgl Lahir</th>
       <th>Anak Ke</th>
       <th>Alamat</th>
       <th>Asal Sekolah</th>
       <th>Nama Ayah</th>
       <th>Nama Ibu</th>
       <th>Alamat Ortu</th>
       <th>Telepon Ortu</th>
       <th>EMail Ortu</th>
       </tr>
      ';
      foreach($data->result() as $row){
        $output .= '
        <tr>
        <td>'.$row->nis.'</td>
        <td>'.$row->nisn.'</td>
        <td>'.$row->nama_siswa.'</td>
        <td>'.$row->jk.'</td>
        <td>'.$row->tmp_lahir.'</td>
        <td>'.$row->tgl_lahir.'</td>
        <td>'.$row->anakke.'</td>
        <td>'.$row->alamat.'</td>
        <td>'.$row->asal_sek.'</td>
        <td>'.$row->ortu_ayah.'</td>
        <td>'.$row->ortu_ibu.'</td>
        <td>'.$row->ortu_alamat.'</td>
        <td>'.$row->ortu_telp.'</td>
        <td>'.$row->ortu_email.'</td>
        </tr>
        ';
      }
      $output .= '</table>';
      echo $output;
    }
    function import()
    {
      if(isset($_FILES["file"]["name"])){
        $path = $_FILES["file"]["tmp_name"];
        $object = PHPExcel_IOFactory::load($path);
        foreach($object->getWorksheetIterator() as $worksheet){
          $highestRow = $worksheet->getHighestRow();
          $highestColumn = $worksheet->getHighestColumn();
          for($row=14; $row<=$highestRow; $row++){
             $nis = $worksheet->getCellByColumnAndRow(0, $row)->getValue();
             $nisn = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
             $nama_siswa = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
             $jk = $worksheet->getCellByColumnAndRow(3, $row)->getValue();
             $tmp_lahir = $worksheet->getCellByColumnAndRow(4, $row)->getValue();
             $tgl_lahir = $worksheet->getCellByColumnAndRow(5, $row)->getValue();
             $anakke = $worksheet->getCellByColumnAndRow(6, $row)->getValue();
             $alamat = $worksheet->getCellByColumnAndRow(7, $row)->getValue();
             $asal_sek = $worksheet->getCellByColumnAndRow(8, $row)->getValue();
             $ortu_ayah = $worksheet->getCellByColumnAndRow(9, $row)->getValue();
             $ortu_ibu = $worksheet->getCellByColumnAndRow(10, $row)->getValue();
             $ortu_alamat = $worksheet->getCellByColumnAndRow(11, $row)->getValue();
             $ortu_telp = $worksheet->getCellByColumnAndRow(12, $row)->getValue();
             $ortu_email = $worksheet->getCellByColumnAndRow(13, $row)->getValue();
             $data[] = array(
              'nis'  => $nis,
              'nisn'   => $nisn,
              'nama_siswa'   => $nama_siswa,
              'jk'   => $jk,
              'tmp_lahir'   => $tmp_lahir,
              'tgl_lahir'   => $tgl_lahir,
              'anakke'   => $anakke,
              'alamat'   => $alamat,
              'asal_sek'   => $asal_sek,
              'ortu_ayah'   => $ortu_ayah,
              'ortu_ibu'   => $ortu_ibu,
              'ortu_alamat'   => $ortu_alamat,
              'ortu_telp'   => $ortu_telp,
              'ortu_email'   => $ortu_email,
             );
          }
        }
        $this->Siswa_model->import($data);
        $this->session->set_flashdata('success', 'Data Imported successfully!');
        redirect('siswa');
      }
    }


    public function cetak()
    {
      $data['siswa'] = $this->Siswa_model->view();
      $data['sekolah'] = $this->Profil_sekolah_model->view();
      $this->load->library('pdf');
      $this->pdf->setPaper('A4', 'landscape');
      $this->pdf->filename = "laporan-data-siswa.pdf";
      $this->pdf->load_view('admin/siswa/laporan_siswa', $data);

    }
}
