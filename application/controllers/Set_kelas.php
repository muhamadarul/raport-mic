<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Set_kelas extends CI_Controller
{
  public function __construct()
  {
      parent::__construct();
          $this->load->model('Login_model');
          $this->load->model('Tahun_ajaran_model');
          $this->load->model('Kelas_model');
          $this->load->model('Siswa_model');
          $this->load->model('Guru_model');
          if (!$this->session->userdata('username')) {
                  $this->session->set_flashdata('error', 'Anda belum melakukan login!');
                  redirect('login');
              }

          if ($this->session->userdata('akses') == 3) {
                  redirect('blank');
          }

  }

  public function index()
  {
    $data['user'] = $this->Login_model->success_login();
    $data['tahun'] = $this->Tahun_ajaran_model->view();
    $data['kelas'] = $this->Kelas_model->view();
    $data['wali'] = $this->Guru_model->view();
    $data['title'] = 'Setting Kelas Siswa';
    // echo 'Selamat datang ' . $data['user']['nama'];
    $this->load->view('templates/header', $data);
    $this->load->view('admin/set_kelas/index', $data);
    $this->load->view('templates/footer');
  }

  public function ambil_siswa()
  {
    $siswa= $this->input->get('siswa');
    $query = $this->Siswa_model->getSiswa($siswa,'nama_siswa');
    echo json_encode($query);
  }

  public function tambahkelas()
  {
    $nama_kelas = $this->input->post('nama_kelas');
    $tahun_ajaran = $this->input->post('tahun_ajaran');
    $wali_kelas = $this->input->post('wali_kelas');

    if ($nama_kelas=='') {
      $result['pesan'] ="Nama Kelas Harus Diisi";
    } elseif ($tahun_ajaran=='') {
        $result['pesan'] ="Tahun Ajaran Harus Diisi";
    } elseif ($wali_kelas=='') {
        $result['pesan'] ="Wali Kelas Harus Diisi";
    } else {
      $result['pesan'] ="";
      $data = array(
        'nama_kelas' => $nama_kelas ,
        'tahun_ajaran' => $tahun_ajaran ,
        'wali_kelas' => $wali_kelas
      );
      $this->Kelas_model->tambah($data);
    }
    echo json_encode($result);
  }
  public function ambilkelas(){
    $ambil_id = $this->Kelas_model->getLastKelas();
    echo json_encode($ambil_id);
  }

  public function tampilsiswa()
  {
    $data['kelas'] =$this->Kelas_model->getLastKelas();
    $id =  $data['kelas']['id_kelas'];
    $siswa = $this->Kelas_model->SiswaKelas($id);
    echo json_encode($siswa);
    // var_dump($siswa);
  }
  public function tampilsiswa_edit()
  {

    $id = $this->input->post('id');
    $siswa = $this->Kelas_model->SiswaKelas($id);
    echo json_encode($siswa);
    // var_dump($siswa);
  }

  public function tambahsiswa()
  {
    $kelas = $this->input->post('kelas');
    $itemSiswa= $this->input->post('itemSiswa');
    $if_exist = $this->Kelas_model->checkExistSiswa($kelas,$itemSiswa);
    if ($itemSiswa=='') {
      $result['pesan'] ="Nama Siswa Harus Diisi";
    } elseif ($kelas=='') {
      $result['pesan'] ="Form Kelas harus diisi";
    } elseif ($if_exist > 0) {
        $result['pesan'] ="Siswa untuk Kelas ini sudah ada";
    }else{
      $result['pesan'] ='';
      $data = array(
        'kelas' => $kelas ,
        'siswa' => $itemSiswa
      );
      $this->Kelas_model->tambahSiswaKelas($data);
    }
    echo json_encode($result);
  }
  public function hapussiswa()
  {
    $id = $this->input->post('id_kelas_detail');
    $this->Kelas_model->hapusSiswaKelas($id);

  }
}
