<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Login_model');
        $this->load->model('User_model');
        $this->load->helper('url');
        if (!$this->session->userdata('username')) {
                $this->session->set_flashdata('error', 'Anda belum melakukan login!');
                redirect('login');
        }
        if ($this->session->userdata('akses') == 2 || $this->session->userdata('akses') == 3) {
                redirect('blank');
        }
    }

    public function index()
    {
        $data['user'] = $this->Login_model->success_login();
        $data['title'] = 'Manajemen user';
        $data['view'] = $this->User_model->view();
        // echo 'Selamat datang ' . $data['user']['nama'];
        $this->load->view('templates/header', $data);
        $this->load->view('admin/user/index', $data);
        $this->load->view('templates/footer');
    }

    public function tambah()
    {
      $data['user'] = $this->Login_model->success_login();
      $data['akses'] = $this->User_model->viewAkses();
      $data['title'] = 'Tambah Admin';

      $this->form_validation->set_rules('nama', 'Nama', 'required|trim', [
          'required' => 'nama tidak boleh kosong!'
      ]);
      $this->form_validation->set_rules('username', 'Username', 'required|trim|is_unique[m_user.username]', [
          'required' => 'Username tidak boleh kosong!',
          'is_unique' => 'Username sudah digunakan!'
      ]);
      $this->form_validation->set_rules('password1', 'Password', 'required|trim|min_length[6]|matches[password2]', [
          'matches' => 'Password tidak sesuai!',
          'min_length' => 'Password terlalu pendek!',
          'required' => 'Password tidak boleh kosong'
      ]);
      $this->form_validation->set_rules('password2', 'password', 'required|trim|matches[password1]');

      if ($this->form_validation->run() == false) {
          $this->load->view('templates/header', $data);
          $this->load->view('admin/user/tambah', $data);
          $this->load->view('templates/footer');

      } else {
          $this->User_model->tambah();
          $this->session->set_flashdata('success', 'Data berhasil ditambahkan!');
          redirect('user');
      }
    }

    public function hapus($id)
    {
        $this->User_model->hapus($id);
        $this->session->set_flashdata('success', 'Data berhasil dihapus');
        redirect('user');
    }

    public function ubah($id = null)
    {
        $data['user'] = $this->Login_model->success_login();
        $data['admin'] = $this->User_model->getById($id);
        $data['akses'] = $this->User_model->viewAkses();
        $data['title'] = 'Ubah Admin';

        $this->form_validation->set_rules('nama', 'Nama', 'required|trim', [
            'required' => 'nama tidak boleh kosong!'
        ]);
        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data);
            $this->load->view('admin/user/ubah', $data);
            $this->load->view('templates/footer');
        } else {
            $this->User_model->ubah();
            $this->session->set_flashdata('success', 'Data berhasil diubah!');
            redirect('user');
        }
    }
    public function detail($id = null)
    {
        $data['user'] = $this->Login_model->success_login();
        $data['admin'] = $this->User_model->getById($id);
        $data['title'] = 'Admin | Detail Admin';
            $this->load->view('templates/header', $data);
            $this->load->view('admin/user/detail', $data);
            $this->load->view('templates/footer');

    }
}
