<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Profil_sekolah extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Login_model');
        $this->load->model('Profil_sekolah_model');
        if (!$this->session->userdata('username')) {
                $this->session->set_flashdata('error', 'Anda belum melakukan login!');
                redirect('login');
        }
        if ($this->session->userdata('akses') == 3) {
                redirect('blank');
        }
    }

    public function index()
    {
        $data['user'] = $this->Login_model->success_login();
        $data['view'] = $this->Profil_sekolah_model->view();
        $data['title'] = 'Profil Sekolah';
        $this->form_validation->set_rules('nama_sekolah', 'Nama sekolah', 'required|trim', [
            'required' => 'Nama Sekolah tidak boleh kosong!'
        ]);
        $this->form_validation->set_rules('kepsek', 'Nama Kepala Sekolah', 'required|trim', [
            'required' => 'Nama Kepala Sekolah tidak boleh kosong!'
        ]);
        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data);
            $this->load->view('admin/profil_sekolah/index', $data);
            $this->load->view('templates/footer');
        } else {
            $this->Profil_sekolah_model->ubah();
            $this->session->set_flashdata('success', 'Data berhasil diubah!');
            redirect('beranda');
        }

    }

}
