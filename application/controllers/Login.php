<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

  public function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('Login_model');
    $this->load->model('Profil_sekolah_model');
    $this->load->helper('url');
	}

	public function index()
	{
    $data['sekolah'] = $this->Profil_sekolah_model->view();
    $this->form_validation->set_rules('username', 'Username', 'required', [
			'required' => 'Username tidak boleh kosong!'
		]);
		$this->form_validation->set_rules('password', 'Password', 'required|trim', [
			'required' => 'Password tidak boleh kosong'
		]);

		if ($this->form_validation->run() == false) {
			$data['title'] = 'Login Pengguna';

			$this->load->view('templates/auth_header', $data);
			$this->load->view('login/index',$data);
			$this->load->view('templates/auth_footer');
		} else {
			$username = $this->input->post('username');
			$password = $this->input->post('password');

			$user = $this->Login_model->login($username, $password);

			if ($user) {
				//jika usernya sudah aktif
				if ($user['aktif'] == 1) {
					//cek password
					if (password_verify($password, $user['password'])) {
						$data = [
							'username' => $user['username'],
            	'akses' => $user['akses']
						];
						$this->session->set_userdata($data);
            $this->session->set_flashdata('success', 'Login berhasil!');
						redirect('beranda');
					} else {
						$this->session->set_flashdata('warning', 'Password salah, periksa kembali');
						redirect('login');
					}
				} else {
					$this->session->set_flashdata('warning', 'Username belum diaktivasi');
					redirect('login');
				}
			} else {
				$this->session->set_flashdata('error', 'Username belum terdaftar!');
				redirect('login');
			}
		}
	}

  public function logout()
  {

    $this->session->unset_userdata('username');
    $this->session->unset_userdata('akses');
    $this->session->set_flashdata('success', 'Anda telah logout');
    redirect('login');
  }


}
