<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mapel extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Login_model');
        $this->load->model('Mapel_model');
        $this->load->model('Kelompok_mapel_model');
        $this->load->model('Profil_sekolah_model');
        $this->load->helper('url');
        if (!$this->session->userdata('username')) {
                $this->session->set_flashdata('error', 'Anda belum melakukan login!');
                redirect('login');
        }
        if ($this->session->userdata('akses') == 3) {
                redirect('blank');
        }
    }

    public function index()
    {
        $data['user'] = $this->Login_model->success_login();
        $data['title'] = 'Mata Pelajaran';
        $data['view'] = $this->Mapel_model->viewAll();
        // var_dump($data['view']);
        // echo 'Selamat datang ' . $data['user']['nama'];
        $this->load->view('templates/header', $data);
        $this->load->view('admin/mapel/index', $data);
        $this->load->view('templates/footer');
    }

    public function tambah()
    {
      $data['user'] = $this->Login_model->success_login();
      $data['kelmapel'] = $this->Kelompok_mapel_model->view();
      $data['title'] = 'Tambah Mata Pelajaran ';

      $this->form_validation->set_rules('nama_mapel', 'Mata Pelajaran', 'required|trim', [
          'required' => 'Nama Mata Pelajaran tidak boleh kosong!'
      ]);
      if ($this->form_validation->run() == false) {
          $this->load->view('templates/header', $data);
          $this->load->view('admin/mapel/tambah', $data);
          $this->load->view('templates/footer');
      } else {
          $this->Mapel_model->tambah();
          $this->session->set_flashdata('success', 'Data berhasil ditambahkan!');
          redirect('mapel');
      }
    }

    public function hapus($id)
    {
      if ($this->session->userdata('akses') == 2) {
              redirect('blank');
      }
        $this->Mapel_model->hapus($id);
        $this->session->set_flashdata('success', 'Data berhasil dihapus');
        redirect('mapel');
    }

    public function ubah($id = null)
    {
        $data['user'] = $this->Login_model->success_login();
        $data['view'] = $this->Mapel_model->getById($id);
        $data['kelmapel'] = $this->Kelompok_mapel_model->view();
        $data['title'] = 'Ubah Mata Pelajaran';
        $this->form_validation->set_rules('nama_mapel', 'Mata Pelajaran', 'required|trim', [
            'required' => 'Nama Mata Pelajaran tidak boleh kosong!'
        ]);
        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data);
            $this->load->view('admin/mapel/ubah', $data);
            $this->load->view('templates/footer');
        } else {
            $this->Mapel_model->ubah();
            $this->session->set_flashdata('success', 'Data berhasil diubah!');
            redirect('mapel');
        }
    }

    public function cetak()
    {
      $data['mapel'] = $this->Mapel_model->viewAll();
      $data['sekolah'] = $this->Profil_sekolah_model->view();
      $this->load->library('pdf');
      $this->pdf->setPaper('A4', 'landscape');
      $this->pdf->filename = "laporan-data-mapel.pdf";
      $this->pdf->load_view('admin/mapel/laporan', $data);
    }
}
