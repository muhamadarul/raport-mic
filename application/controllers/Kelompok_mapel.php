<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kelompok_mapel extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Login_model');
        $this->load->model('Kelompok_mapel_model');
        $this->load->helper('url');
        if (!$this->session->userdata('username')) {
                $this->session->set_flashdata('error', 'Anda belum melakukan login!');
                redirect('login');
        }
        if ($this->session->userdata('akses') == 3) {
                redirect('blank');
        }
    }

    public function index()
    {
        $data['user'] = $this->Login_model->success_login();
        $data['title'] = 'Kelompok Mapel';
        $data['view'] = $this->Kelompok_mapel_model->view();
        // echo 'Selamat datang ' . $data['user']['nama'];
        $this->load->view('templates/header', $data);
        $this->load->view('admin/kelmapel/index', $data);
        $this->load->view('templates/footer');
    }

    public function tambah()
    {
      $data['user'] = $this->Login_model->success_login();
      $data['title'] = 'Tambah Kelompok Mapel';

      $this->form_validation->set_rules('nama_kelmapel', 'Kelompok Mata Pelajaran', 'required|trim', [
          'required' => 'Kelompok Mata Pelajaran tidak boleh kosong!'
      ]);
      if ($this->form_validation->run() == false) {
          $this->load->view('templates/header', $data);
          $this->load->view('admin/kelmapel/tambah', $data);
          $this->load->view('templates/footer');
      } else {
          $this->Kelompok_mapel_model->tambah();
          $this->session->set_flashdata('success', 'Data berhasil ditambahkan!');
          redirect('kelompok_mapel');
      }
    }

    public function hapus($id)
    {
      if ($this->session->userdata('akses') == 2) {
              redirect('blank');
      }
        $this->Kelompok_mapel_model->hapus($id);
        $this->session->set_flashdata('success', 'Data berhasil dihapus');
        redirect('kelompok_mapel');
    }

    public function ubah($id = null)
    {
        $data['user'] = $this->Login_model->success_login();
        $data['view'] = $this->Kelompok_mapel_model->getById($id);
        $data['title'] = 'Ubah Kelompok Mata Pelajaran';
        $this->form_validation->set_rules('nama_kelmapel', 'Kelompok Mata Pelajaran', 'required|trim', [
            'required' => 'Kelompok Mata Pelajaran tidak boleh kosong!'
        ]);
        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data);
            $this->load->view('admin/kelmapel/ubah', $data);
            $this->load->view('templates/footer');
        } else {
            $this->Kelompok_mapel_model->ubah();
            $this->session->set_flashdata('success', 'Data berhasil diubah!');
            redirect('kelompok_mapel');
        }
    }
}
