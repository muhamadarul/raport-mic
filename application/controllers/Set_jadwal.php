<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Set_jadwal extends CI_Controller
{
  public function __construct()
  {
      parent::__construct();
          $this->load->model('Login_model');
          $this->load->model('Mapel_model');
          $this->load->model('Kelas_model');
          $this->load->model('Jadwal_model');
          $this->load->model('Guru_model');

          if (!$this->session->userdata('username')) {
                  $this->session->set_flashdata('error', 'Anda belum melakukan login!');
                  redirect('login');
              }

          if ($this->session->userdata('akses') == 3) {
                  redirect('blank');
          }
  }

  public function index()
  {
    $data['user'] = $this->Login_model->success_login();
    $data['title'] = 'Setting Jadwal Mapel';
    // $data['mapel'] = $this->Mapel_model->view();
    $data['kelas'] = $this->Kelas_model->viewKelasTa();
    $data['hari'] = $this->Jadwal_model->viewHari();
    $data['guru'] = $this->Guru_model->view();
    $this->load->view('templates/header', $data);
    $this->load->view('admin/set_jadwal/index', $data);
    $this->load->view('templates/footer');
  }


  public function tambahjadwal()
  {
    $kelas = $this->input->post('kelas');
    $if_exist = $this->Jadwal_model->checkExist($kelas);
    $nama_jadwal = $this->input->post('nama_jadwal');
    if ($kelas=='') {
      $result['pesan'] ="Kelas Harus Diisi";
    }elseif ($nama_jadwal=='') {
      $result['pesan'] ="Nama Jadwal Harus Diisi";
    } elseif ($if_exist > 0) {
        $result['pesan'] ="Jadwal untuk Kelas ini sudah ada";
    }else {
      $result['pesan'] ="";
      $data = array(
        'kelas' => $kelas ,
        'nama_jadwal' => $nama_jadwal
      );
      $this->Jadwal_model->tambah($data);
    }
    echo json_encode($result);
  }

  public function ambiljadwal(){
    $ambil_id = $this->Jadwal_model->getLastJadwal();
    echo json_encode($ambil_id);
  }

  public function ambilmapel()
  {
    $mapel= $this->input->get('mapel');
    $query = $this->Mapel_model->getMapel($mapel,'nama_mapel');
    echo json_encode($query);
  }

  public function selectedmapel()
  {
    $id = $this->input->post('id_m');
    $query = $this->Mapel_model->getSelectedMapel($id);
    echo json_encode($query);
  }

  public function ambil_id_detail()
  {
    $id = $this->input->post('id');
    $jadwaldetail =  $this->Jadwal_model->getIdDetail($id);
    echo json_encode($jadwaldetail);
  }

  public function tampiljadwaldetail()
  {
    $data['jadwal'] = $this->Jadwal_model->getLastJadwal();
    $id =  $data['jadwal']['id_jadwal'];
    $jadwal = $this->Jadwal_model->JadwalDetail($id);
    echo json_encode($jadwal);
  }
  public function tampiljadwaldetailedit()
  {
    $id =  $this->input->post('id_jadwal');
    $jadwal = $this->Jadwal_model->JadwalDetail($id);
    echo json_encode($jadwal);
  }

  public function tambahjadwaldetail()
  {
    $jadwal = $this->input->post('jadwal');
    $itemMapel = $this->input->post('itemMapel');
    $hari = $this->input->post('hari');
    $jam = $this->input->post('jam');
    $pengampu = $this->input->post('pengampu');
    $if_exist = $this->Jadwal_model->detailcheckExist($itemMapel,$jadwal);

    if ($itemMapel=='') {
      $result['pesan'] ="Mata Pelajaran Harus Diisi";
    } elseif ($jadwal=='') {
      $result['pesan'] ="Form Jadwal harus diisi";
    } elseif ($jam=='') {
      $result['pesan'] ="Jam Jadwal harus diisi";
    } elseif ($hari=='') {
      $result['pesan'] ="Hari Jadwal harus diisi";
    } elseif ($pengampu=='') {
      $result['pesan'] ="Pengampu Mapel harus diisi";
    } elseif ($if_exist > 0) {
        $result['pesan'] ="Mata Pelajaran untuk Jadwal ini sudah ada";
    }else{
      $result['pesan'] ='';
      $data = array(
        'jadwal' => $jadwal,
        'mapel' => $itemMapel,
        'hari' => $hari,
        'jam' => $jam,
        'pengampu' => $pengampu,
      );
      $this->Jadwal_model->tambahJadwalDetail($data);
    }
    echo json_encode($result);
  }

  public function ubahjadwaldetail()
  {
    $id = $this->input->post('id_jdetail');
    $hari = $this->input->post('hari2');
    $jam = $this->input->post('jam2');
    $pengampu = $this->input->post('pengampu2');
    // $jadwal = $this->input->post('jadwal');
    // $itemMapel = $this->input->post('itemMapel2');
    // $if_exist = $this->Jadwal_model->detailcheckExist($itemMapel,$jadwal);

    // if ($itemMapel=='') {
    //   $result['pesan'] ="Mata Pelajaran Harus Diisi";
    if ($id=='') {
      $result['pesan'] ="Form Jadwal harus diisi";
    } elseif ($jam=='') {
      $result['pesan'] ="Jam Jadwal harus diisi";
    } elseif ($hari=='') {
      $result['pesan'] ="Hari Jadwal harus diisi";
    } elseif ($pengampu=='') {
      $result['pesan'] ="Pengampu Mapel harus diisi";
    // } elseif ($if_exist > 0) {
    //     $result['pesan'] ="Mata Pelajaran untuk Jadwal ini sudah ada";
    }else{
      $result['pesan'] ='';
      $where = array('id_jadwal_detail' =>$id);
      $data = array(
        // 'mapel' => $itemMapel,
        'hari' => $hari,
        'jam' => $jam,
        'pengampu' => $pengampu,
      );
      $this->Jadwal_model->ubahJadwalDetail($where,$data);
    }
    echo json_encode($result);
  }

  public function hapusjadwaldetail()
  {
    $id = $this->input->post('id_jadwal_detail');
    $this->Jadwal_model->hapusJadwalDetail($id);
  }
}
