<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kelas extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Login_model');
        $this->load->model('Kelas_model');
        $this->load->model('Tahun_ajaran_model');
        $this->load->model('Guru_model');
        $this->load->helper('url');
        $this->load->model('Profil_sekolah_model');
        if (!$this->session->userdata('username')) {
                $this->session->set_flashdata('error', 'Anda belum melakukan login!');
                redirect('login');
        }
        if ($this->session->userdata('akses') == 3) {
                redirect('blank');
        }
    }

    public function index()
    {
        $data['user'] = $this->Login_model->success_login();
        $data['title'] = 'Kelas';
        $data['view'] = $this->Kelas_model->viewAll();
        // echo 'Selamat datang ' . $data['user']['nama'];
        $this->load->view('templates/header', $data);
        $this->load->view('admin/kelas/index', $data);
        $this->load->view('templates/footer');
    }
    public function ambilid()
    {
      $id=$this->input->post('id_kelas');
      $kelas=$this->Kelas_model->ambilid($id);
      echo json_encode($kelas);
    }
    public function ubah($id = null)
    {
        $data['user'] = $this->Login_model->success_login();
        $data['view'] = $this->Kelas_model->getById($id);
        $data['tahun'] = $this->Tahun_ajaran_model->view();
        $data['guru'] = $this->Guru_model->view();
        $data['title'] = 'Ubah Kelas';
        $this->load->view('templates/header', $data);
        $this->load->view('admin/kelas/ubah', $data);
        $this->load->view('templates/footer');
    }
    public function detail($id = null)
    {
        $data['user'] = $this->Login_model->success_login();
        $data['view'] = $this->Kelas_model->getById($id);
        $data['siswa'] = $this->Kelas_model->detailSiswaKelas($id);
        $data['title'] = 'Datail Kelas';
        $this->load->view('templates/header', $data);
        $this->load->view('admin/kelas/detail', $data);
        $this->load->view('templates/footer');
    }
    public function ubahkelas()
    {
      $id =  $this->input->post('id_kelas');
      $nama_kelas = $this->input->post('nama_kelas');
      $tahun_ajaran = $this->input->post('tahun_ajaran');
      $wali_kelas = $this->input->post('wali_kelas');

      if ($nama_kelas=='') {
        $result['pesan'] ="Nama Kelas Harus Diisi";
      } elseif ($tahun_ajaran=='') {
          $result['pesan'] ="Tahun Ajaran Harus Diisi";
      } elseif ($wali_kelas=='') {
          $result['pesan'] ="Wali Kelas Harus Diisi";
      } else {
        $result['pesan'] ="";
        $where = array('id_kelas' => $id);
        $data = array(
          'nama_kelas' => $nama_kelas ,
          'tahun_ajaran' => $tahun_ajaran ,
          'wali_kelas' => $wali_kelas
        );
        $this->Kelas_model->ubah($where,$data);
      }
      echo json_encode($result);
    }

    public function hapus($id)
    {
      if ($this->session->userdata('akses') == 2) {
              redirect('blank');
      }
        $this->Kelas_model->hapus($id);
        $this->session->set_flashdata('success', 'Data berhasil dihapus');
        redirect('kelas');
    }

    public function cetak($id = null)
    {
      $data['view'] = $this->Kelas_model->getById($id);
      $data['siswa'] = $this->Kelas_model->detailSiswaKelas($id);
      $data['sekolah'] = $this->Profil_sekolah_model->view();
      $nama=$data['view']['nama_kelas'];
      $ta=$data['view']['thn_ajaran'];
      $this->load->library('pdf');
      $this->pdf->setPaper('A4', 'landscape');
      $this->pdf->filename = "laporan-data-kelas-'$nama'.pdf";
      $this->pdf->load_view('admin/kelas/laporan', $data);
    }


}
