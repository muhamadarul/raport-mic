<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Catatan_uts extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Login_model');
        $this->load->model('Kelas_model');
        $this->load->model('Siswa_model');
        $this->load->model('Tahun_ajaran_model');
        $this->load->model('Catatan_uts_model');
        $this->load->model('Kelompok_mapel_model');
        $this->load->helper('url');
        if (!$this->session->userdata('username')) {
                $this->session->set_flashdata('error', 'Anda belum melakukan login!');
                redirect('login');
        }
    }
    public function index()
    {
        $data['user'] = $this->Login_model->success_login();
        $data['title'] = 'Catatan Walikelas UTS';
        $data['tahun'] = $this->Tahun_ajaran_model->view();
        $this->load->view('templates/header', $data);
        $this->load->view('admin/catatan_uts/index', $data);
        $this->load->view('templates/footer');
    }
    public function view()
    {
      $data = $this->Catatan_uts_model->view();
      echo json_encode($data);
    }
    public function ambilkelas()
    {
      $id = $this->input->post('id_ta');
      $query = $this->Kelas_model->getSelectedKelasTa($id);
      echo json_encode($query);
    }
    public function ambilid()
    {
      $ambil_id = $this->Catatan_uts_model->getLastId();
      echo json_encode($ambil_id);
    }
    public function svd_nilai()
    {
      $id = $this->input->post('id');
      $kelas=$this->input->post('id_kelas');
      $siswa = $this->Kelas_model->getSiswaByKelas($kelas);
      $result = array();
      $index=0;
        if (count($siswa) > 0) {
          foreach ($siswa as $key => $val) {
            $result[] = array(
              'catatan_uts' => $id,
              'siswa'=>$val['siswa']
            );
          }
        }
     $this->Catatan_uts_model->SaveNilaiDetail($result);
    }
    public function ubahnilai()
    {
      $id = $this->input->post('id_nilai');
      $catatan =  $this->input->post('catatan');
      for ($count=0; $count < count($id); $count++) {
        $this->db->where('id_catatan_uts_detail', $id[$count]);
           $this->db->update('t_catatan_uts_detail', [
               'catatan_uts' => $catatan[$count],
          ]);
      }
    }
    public function nilaisiswa()
    {
      $id= $this->input->post('id');
      $nilai = $this->Catatan_uts_model->viewNilaiById($id);
      echo json_encode($nilai);
    }
    public function view_thnkelas()
    {
      $id = $this->input->post('id_kehadiran');
      $result = $this->Catatan_uts_model->viewKelas($id);
      echo json_encode($result);
    }
    public function tambah()
    {
      $ta = $this->input->post('ta');
      $kelas= $this->input->post('kelas');
      $semester = $this->input->post('semester');
      $if_exist = $this->Catatan_uts_model->checkExist($ta,$kelas,$semester);
      if ($ta=='') {
        $result['pesan'] ="Tahun Ajaran Harus dipilih";
      } elseif ($kelas=='') {
        $result['pesan'] ="Kelas harus dipilih";
      } elseif ($semester=='') {
        $result['pesan'] ="Semester harus dipilih";
      } elseif ($if_exist > 0) {
          $result['pesan'] ="Catatan Wali Kelas Untuk Kelas Ini Sudah Ada !";
      }else{
        $result['pesan'] ='';
        $data = array(
          'ta' => $ta,
          'kelas' => $kelas ,
          'semester' => $semester
        );
        $this->Catatan_uts_model->tambah($data);
      }
      echo json_encode($result);
    }
    public function hapus()
    {
        $id = $this->input->post('id');
        $this->Catatan_uts_model->hapus($id);
    }
}
