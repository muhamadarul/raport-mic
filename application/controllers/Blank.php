<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Blank extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Login_model');
        if (!$this->session->userdata('username')) {
                $this->session->set_flashdata('error', 'Anda belum melakukan login!');
                redirect('login');
            }
    }

    public function index()
    {

        $data['title'] = 'Access Denied';
        $data['user'] = $this->Login_model->success_login();
        // echo 'Selamat datang ' . $data['user']['nama'];
        $this->load->view('templates/header', $data);
        $this->load->view('errors/access_denied', $data);
        $this->load->view('templates/footer');
    }
}
