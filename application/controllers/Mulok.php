<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mulok extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Login_model');
        $this->load->model('Mulok_model');
        $this->load->helper('url');
        if (!$this->session->userdata('username')) {
                $this->session->set_flashdata('error', 'Anda belum melakukan login!');
                redirect('login');
        }
        if ($this->session->userdata('akses') == 3) {
                redirect('blank');
        }
    }

    public function index()
    {
        $data['user'] = $this->Login_model->success_login();
        $data['title'] = 'Muatan lokal';
        $data['view'] = $this->Mulok_model->view();
        // echo 'Selamat datang ' . $data['user']['nama'];
        $this->load->view('templates/header', $data);
        $this->load->view('admin/mulok/index', $data);
        $this->load->view('templates/footer');
    }

    public function tambah()
    {
      $data['user'] = $this->Login_model->success_login();
      $data['title'] = 'Tambah Muatan Lokal ';

      $this->form_validation->set_rules('mulok', 'Muatan Lokal', 'required|trim', [
          'required' => 'Muatan Lokal tidak boleh kosong!'
      ]);
      if ($this->form_validation->run() == false) {
          $this->load->view('templates/header', $data);
          $this->load->view('admin/mulok/tambah', $data);
          $this->load->view('templates/footer');
      } else {
          $this->Mulok_model->tambah();
          $this->session->set_flashdata('success', 'Data berhasil ditambahkan!');
          redirect('mulok');
      }
    }

    public function hapus($id)
    {
      if ($this->session->userdata('akses') == 2) {
              redirect('blank');
      }
        $this->Mulok_model->hapus($id);
        $this->session->set_flashdata('success', 'Data berhasil dihapus');
        redirect('mulok');
    }

    public function ubah($id = null)
    {
        $data['user'] = $this->Login_model->success_login();
        $data['view'] = $this->Mulok_model->getById($id);
        $data['title'] = 'Ubah Tahun Ajaran';
        $this->form_validation->set_rules('mulok', 'Muatan Lokal', 'required|trim', [
            'required' => 'Muatan Lokal tidak boleh kosong!'
        ]);

        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data);
            $this->load->view('admin/mulok/ubah', $data);
            $this->load->view('templates/footer');
        } else {
            $this->Mulok_model->ubah();
            $this->session->set_flashdata('success', 'Data berhasil diubah!');
            redirect('mulok');
        }
    }
    public function cetak()
    {
      $data['mulok'] = $this->Mulok_model->view();
      $this->load->library('pdf');
      $this->pdf->setPaper('A4', 'potrait');
      $this->pdf->filename = "laporan-data-mulok.pdf";
      $this->pdf->load_view('admin/mulok/laporan_mulok', $data);

    }
}
