<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Jadwal extends CI_Controller
{
    public function __construct()
    {
      parent::__construct();
          $this->load->model('Login_model');
          $this->load->model('Mapel_model');
          $this->load->model('Kelas_model');
          $this->load->model('Jadwal_model');
          $this->load->model('Guru_model');
          $this->load->model('Profil_sekolah_model');
          if (!$this->session->userdata('username')) {
                  $this->session->set_flashdata('error', 'Anda belum melakukan login!');
                  redirect('login');
              }

          if ($this->session->userdata('akses') == 3) {
                  redirect('blank');
          }
    }

    public function index()
    {
        $data['user'] = $this->Login_model->success_login();
        $data['title'] = 'Jadwal';
        $data['view'] = $this->Jadwal_model->viewAll();
        // echo 'Selamat datang ' . $data['user']['nama'];
        $this->load->view('templates/header', $data);
        $this->load->view('admin/jadwal/index', $data);
        $this->load->view('templates/footer');
    }
    public function ambilid()
    {
      $id=$this->input->post('id_kelas');
      $kelas=$this->Kelas_model->ambilid($id);
      echo json_encode($kelas);
    }
    public function ubah($id = null)
    {
        $data['user'] = $this->Login_model->success_login();
        // $data['mapel'] = $this->Mapel_model->view();
        $data['view'] = $this->Jadwal_model->getById($id);
        $data['kelas'] = $this->Kelas_model->viewKelasTa();
        $data['hari'] = $this->Jadwal_model->viewHari();
        $data['guru'] = $this->Guru_model->view();
        $data['title'] = 'Ubah Jadwal';
        $this->load->view('templates/header', $data);
        $this->load->view('admin/jadwal/ubah', $data);
        $this->load->view('templates/footer');
    }
    public function detail($id = null)
    {
        $data['user'] = $this->Login_model->success_login();
        $data['view'] = $this->Jadwal_model->getById($id);
        $data['jadwal'] = $this->Jadwal_model->JadwalDetail($id);
        $data['title'] = 'Datail Jadwal';
        $this->load->view('templates/header', $data);
        $this->load->view('admin/jadwal/detail', $data);
        $this->load->view('templates/footer');
    }

    public function ubahjadwal()
    {
      $id = $this->input->post('id');
      $nama_jadwal = $this->input->post('nama_jadwal');
      if ($nama_jadwal=='') {
        $result['pesan'] ="Nama Jadwal Harus Diisi";
      }else {
        $result['pesan'] ="";
        $where = array('id_jadwal' => $id);
        $data = array(
          'nama_jadwal' => $nama_jadwal
        );
        $this->Jadwal_model->ubah($where,$data);
      }
      echo json_encode($result);
    }

    public function hapus($id)
    {
      if ($this->session->userdata('akses') == 2) {
              redirect('blank');
      }
        $this->Jadwal_model->hapus($id);
        $this->session->set_flashdata('success', 'Data berhasil dihapus');
        redirect('kelas');
    }

    public function cetak($id = null)
    {
      $data['view'] = $this->Jadwal_model->getById($id);
      $data['sekolah'] = $this->Profil_sekolah_model->view();
      $data['jadwal'] = $this->Jadwal_model->JadwalDetail($id);
      $this->load->library('pdf');
      $this->pdf->setPaper('A4', 'landscape');
      $this->pdf->filename = "jadwal.pdf";
      $this->pdf->load_view('admin/jadwal/laporan', $data);
    }


}
