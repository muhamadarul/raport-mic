<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Guru extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Login_model');
        $this->load->model('Guru_model');
        $this->load->helper('url');
        $this->load->model('Profil_sekolah_model');
        if (!$this->session->userdata('username')) {
                $this->session->set_flashdata('error', 'Anda belum melakukan login!');
                redirect('login');
        }
        if ($this->session->userdata('akses') == 3) {
                redirect('blank');
        }
    }

    public function index()
    {
        $data['user'] = $this->Login_model->success_login();
        $data['title'] = 'Guru';
        $data['view'] = $this->Guru_model->view();
        // echo 'Selamat datang ' . $data['user']['nama'];
        $this->load->view('templates/header', $data);
        $this->load->view('admin/guru/index', $data);
        $this->load->view('templates/footer');
    }

    public function tambah()
    {
      $data['user'] = $this->Login_model->success_login();
      $data['title'] = 'Tambah Guru ';

      $this->form_validation->set_rules('nama_guru', 'Nama Guru', 'required|trim', [
          'required' => 'Nama Guru tidak boleh kosong!'
      ]);

      $this->form_validation->set_rules('no_hp', 'NO HP', 'required|trim', [
          'required' => 'No HP tidak boleh kosong!'
      ]);
      if ($this->form_validation->run() == false) {
          $this->load->view('templates/header', $data);
          $this->load->view('admin/guru/tambah', $data);
          $this->load->view('templates/footer');
      } else {
          $this->Guru_model->tambah();
          $this->session->set_flashdata('success', 'Data berhasil ditambahkan!');
          redirect('guru');
      }
    }

    public function hapus($id)
    {
      if ($this->session->userdata('akses') == 2) {
              redirect('blank');
      }
        $this->Guru_model->hapus($id);
        $this->session->set_flashdata('success', 'Data berhasil dihapus');
        redirect('guru');
    }

    public function ubah($id = null)
    {
        $data['user'] = $this->Login_model->success_login();
        $data['view'] = $this->Guru_model->getById($id);
        $data['title'] = 'Ubah Guru';
        $this->form_validation->set_rules('nama_guru', 'Nama Guru', 'required|trim', [
            'required' => 'Nama Guru tidak boleh kosong!'
        ]);

        $this->form_validation->set_rules('no_hp', 'NO HP', 'required|trim', [
            'required' => 'No HP tidak boleh kosong!'
        ]);
        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data);
            $this->load->view('admin/guru/ubah', $data);
            $this->load->view('templates/footer');
        } else {
            $this->Guru_model->ubah();
            $this->session->set_flashdata('success', 'Data berhasil diubah!');
            redirect('guru');
        }
    }
    public function cetak()
    {
      $data['sekolah'] = $this->Profil_sekolah_model->view();
  		$data['guru'] = $this->Guru_model->view();
  		$this->load->library('pdf');
  		$this->pdf->setPaper('A4', 'landscape');
  		$this->pdf->filename = "laporan-data-guru.pdf";
  		$this->pdf->load_view('admin/guru/laporan_guru', $data);

    }

}
