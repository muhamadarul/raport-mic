<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Beranda extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('Beranda_model');
        $this->load->model('Profil_sekolah_model');
        $this->load->model('Login_model');
        if (!$this->session->userdata('username')) {
                $this->session->set_flashdata('error', 'Anda belum melakukan login!');
                redirect('login');
        }
    }

    public function index()
    {


        $data['user'] = $this->Login_model->success_login();
        $data['hitunguser'] = $this->Beranda_model->hitunguser();
        $data['hitungkelas'] = $this->Beranda_model->hitungkelas();
        $data['hitungmapel'] = $this->Beranda_model->hitungmapel();
        $data['hitungsiswa'] = $this->Beranda_model->hitungsiswa();
        $data['hitungguru'] = $this->Beranda_model->hitungguru();
        $data['hitungeks'] = $this->Beranda_model->hitungeks();
        $data['sekolah'] = $this->Profil_sekolah_model->view();
        $data['title'] = 'Dashboard';
        // echo 'Selamat datang ' . $data['user']['nama'];
        $this->load->view('templates/header', $data);
        $this->load->view('admin/beranda/index', $data);
        $this->load->view('templates/footer');
    }
}
