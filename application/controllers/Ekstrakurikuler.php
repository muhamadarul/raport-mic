<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Ekstrakurikuler extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Login_model');
        $this->load->model('Ekstrakurikuler_model');
        $this->load->model('Profil_sekolah_model');
        $this->load->helper('url');
        if (!$this->session->userdata('username')) {
                $this->session->set_flashdata('error', 'Anda belum melakukan login!');
                redirect('login');
        }
        if ($this->session->userdata('akses') == 3) {
                redirect('blank');
        }
    }

    public function index()
    {
        $data['user'] = $this->Login_model->success_login();
        $data['title'] = 'Ekstrakurikuler';
        $data['view'] = $this->Ekstrakurikuler_model->view();
        // echo 'Selamat datang ' . $data['user']['nama'];
        $this->load->view('templates/header', $data);
        $this->load->view('admin/ekstrakurikuler/index', $data);
        $this->load->view('templates/footer');
    }

    public function tambah()
    {
      $data['user'] = $this->Login_model->success_login();
      $data['title'] = 'Tambah Ekstrakurikuler ';

      $this->form_validation->set_rules('nama_eks', 'Ekstrakurikuler', 'required|trim', [
          'required' => 'Nama Ekstrakurikuler tidak boleh kosong!'
      ]);
      if ($this->form_validation->run() == false) {
          $this->load->view('templates/header', $data);
          $this->load->view('admin/ekstrakurikuler/tambah', $data);
          $this->load->view('templates/footer');
      } else {
          $this->Ekstrakurikuler_model->tambah();
          $this->session->set_flashdata('success', 'Data berhasil ditambahkan!');
          redirect('ekstrakurikuler');
      }
    }

    public function hapus($id)
    {
      if ($this->session->userdata('akses') == 2) {
              redirect('blank');
      }
        $this->Ekstrakurikuler_model->hapus($id);
        $this->session->set_flashdata('success', 'Data berhasil dihapus');
        redirect('ekstrakurikuler');
    }

    public function ubah($id = null)
    {
        $data['user'] = $this->Login_model->success_login();
        $data['view'] = $this->Ekstrakurikuler_model->getById($id);
        $data['title'] = 'Ubah Ekstrakurikuler';
        $this->form_validation->set_rules('nama_eks', 'Ekstrakurikuler', 'required|trim', [
            'required' => 'Nama Ekstrakurikuler tidak boleh kosong!'
        ]);

        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data);
            $this->load->view('admin/ekstrakurikuler/ubah', $data);
            $this->load->view('templates/footer');
        } else {
            $this->Ekstrakurikuler_model->ubah();
            $this->session->set_flashdata('success', 'Data berhasil diubah!');
            redirect('ekstrakurikuler');
        }
    }
    public function cetak()
    {
      $data['eks'] = $this->Ekstrakurikuler_model->view();
      $data['sekolah'] = $this->Profil_sekolah_model->view();
      $this->load->library('pdf');
      $this->pdf->setPaper('A4', 'potrait');
      $this->pdf->filename = "laporan-data-ekstrakurikuler.pdf";
      $this->pdf->load_view('admin/ekstrakurikuler/laporan_eks', $data);

    }

}
