<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title><?= $title; ?></title>

  <!-- Custom fonts for this template-->
  <link href="<?= base_url(); ?>assets/dist/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?= base_url(); ?>assets/plugins/datatables-bs4/css/dataTables.bootstrap4.css">

  <!-- Custom styles for this page -->
  <link href="<?= base_url(); ?>assets/dist/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
  <!-- Custom styles for this template-->
  <link href="<?= base_url(); ?>assets/dist/css/sb-admin-2.min.css" rel="stylesheet">
  <!-- summernote -->
  <link rel="stylesheet" href="<?= base_url(); ?>assets/plugins/summernote/summernote-bs4.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
   <!-- JQuery - Cropper -->
  <link rel="stylesheet" href="<?= base_url(); ?>assets/plugins/cropper-master/cropper.css">

  <link rel="stylesheet" href="<?= base_url(); ?>assets/plugins/select2/css/select2.min.css">
  <link rel="stylesheet" href="<?= base_url(); ?>assets/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
  <!-- SweetAlert2 -->
  <script src="<?= base_url(); ?>assets/plugins/sweetalert/sweetalert2.all.min.js"></script>
  <!-- summernote -->
  <link rel="stylesheet" href="<?= base_url(); ?>assets/plugins/summernote/summernote-bs4.css">
  <!-- Bootstrap core JavaScript-->
  <script src="<?= base_url();?>assets/dist/vendor/jquery/jquery.min.js"></script>
  <script src="<?= base_url();?>assets/dist/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- jQuery UI 1.11.4 -->
  <script src="<?= base_url(); ?>assets/plugins/jquery-ui/jquery-ui.min.js"></script>
  <!--  plugin JavaScript-->
  <!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js" type="text/javascript"></script> -->
  <script src="<?= base_url();?>assets/dist/vendor/jquery-easing/jquery.easing.min.js"></script>
  <!-- Select2 -->
  <script src="<?= base_url(); ?>assets/plugins/select2/js/select2.full.min.js"></script>


</head>
<?php $hal= $this->uri->segment(1);
if ($hal=='kelas') {
  $k= $this->uri->segment(1);
  $u = $this->uri->segment(2);
  $ku = $k.'/'.$u;
}else {
  $ku=false;
  $u=false;
  $ku=false;
}
?>
<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="">
        <div class="sidebar-brand-icon rotate-n-15">
          <i class="far fa-file-alt"></i>
        </div>
          <div class="sidebar-brand-text mx-1">E-Raport</div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
      <li class="nav-item <?=($hal=="beranda")?'active':'';?>">
        <a class="nav-link <?= ($hal=="set_kelas")||($ku=="kelas/ubah") ||($ku=="jadwal/ubah")||($hal=="set_jadwal")? 'tombol-trx' : '' ;?>" href="<?= base_url('beranda'); ?>">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Dashboard</span></a>
      </li>

      <?php if ($user['akses'] == 1 || $user['akses'] == 2) : ?>
      <li class="nav-item <?= ($hal=="siswa")||($hal=="guru")||($hal=="ekstrakurikuler") ||($hal=="kelompok_mapel") ||($hal=="mulok")||($hal=="tahun_ajaran") ||($hal=="mapel") ? 'active' : '' ;?>">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
          <i class="far fa-fw fa-file-alt"></i>
          <span>Master Data</span></a>
          <div id="collapseThree" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
              <a class="collapse-item <?=($hal=="siswa")?'active':'';?> <?= ($hal=="set_kelas")||($ku=="kelas/ubah")||($ku=="jadwal/ubah")||($hal=="set_jadwal") ? 'tombol-trx' : '' ;?>" href="<?= base_url('siswa'); ?>">Data Siswa</a>
              <a class="collapse-item <?=($hal=="guru")?'active':'';?> <?= ($hal=="set_kelas")||($ku=="kelas/ubah")||($ku=="jadwal/ubah")||($hal=="set_jadwal") ? 'tombol-trx' : '' ;?>" href="<?= base_url('guru'); ?>">Data Guru</a>
              <a class="collapse-item <?=($hal=="mapel")?'active':'';?> <?= ($hal=="set_kelas")||($ku=="kelas/ubah")||($ku=="jadwal/ubah")||($hal=="set_jadwal") ? 'tombol-trx' : '' ;?>" href="<?= base_url('mapel'); ?>">Mata Pelajaran</a>
              <a class="collapse-item <?=($hal=="kelompok_mapel")?'active':'';?> <?= ($hal=="set_kelas")||($ku=="kelas/ubah")||($ku=="jadwal/ubah")||($hal=="set_jadwal") ? 'tombol-trx' : '' ;?>" href="<?= base_url('kelompok_mapel'); ?>">Kelompok Mapel</a>
              <a class="collapse-item <?=($hal=="ekstrakurikuler")?'active':'';?> <?= ($hal=="set_kelas") ||($ku=="kelas/ubah")||($ku=="jadwal/ubah")||($hal=="set_jadwal")? 'tombol-trx' : '' ;?>" href="<?= base_url('ekstrakurikuler'); ?>">Ekstrakurikuler</a>
              <a class="collapse-item <?=($hal=="mulok")?'active':'';?> <?= ($hal=="set_kelas")||($ku=="kelas/ubah")||($ku=="jadwal/ubah")||($hal=="set_jadwal") ? 'tombol-trx' : '' ;?>" href="<?= base_url('mulok'); ?>">Muatan lokal</a>
              <a class="collapse-item <?=($hal=="tahun_ajaran")?'active':'';?> <?= ($hal=="set_kelas")||($ku=="kelas/ubah") ||($ku=="jadwal/ubah")||($hal=="set_jadwal")? 'tombol-trx' : '' ;?>" href="<?= base_url('tahun_ajaran'); ?>">Tahun Ajaran</a>
              <a class="collapse-item <?=($hal=="profil_sekolah")?'active':'';?> <?= ($hal=="set_kelas")||($ku=="kelas/ubah") ||($ku=="jadwal/ubah")||($hal=="set_jadwal")? 'tombol-trx' : '' ;?>" href="<?= base_url('profil_sekolah'); ?>">Profil Sekolah</a>
            </div>
          </div>
      </li>
      <li class="nav-item <?= ($hal=="kelas")||($hal=="set_kelas") ? 'active' : '' ;?>">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapse2" aria-expanded="true" aria-controls="collapseThree">
          <i class="far fa-fw fas fa-school"></i>
          <span>Kelas Siswa</span></a>
          <div id="collapse2" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
              <a class="collapse-item <?=($hal=="set_kelas")?'active':'';?> <?= ($hal=="set_kelas")||($ku=="kelas/ubah")||($ku=="jadwal/ubah")||($hal=="set_jadwal") ? 'tombol-trx' : '' ;?>" href="<?= base_url('set_kelas'); ?>">Setting Kelas Siswa</a>
              <a class="collapse-item <?=($hal=="kelas")?'active':'';?> <?= ($hal=="set_kelas")||($ku=="kelas/ubah")||($ku=="jadwal/ubah")||($hal=="set_jadwal") ? 'tombol-trx' : '' ;?>" href="<?= base_url('kelas'); ?>">Data Kelas Siswa</a>
            </div>
          </div>
      </li>
      <li class="nav-item <?= ($hal=="jadwal")||($hal=="set_jadwal") ? 'active' : '' ;?>">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapse3" aria-expanded="true" aria-controls="collapseThree">
          <i class="far fa-fw fas fa-pen"></i>
          <span>Jadwal</span></a>
          <div id="collapse3" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
              <a class="collapse-item <?=($hal=="set_jadwal")?'active':'';?> <?= ($hal=="set_kelas")||($ku=="kelas/ubah") ||($ku=="jadwal/ubah")||($hal=="set_jadwal")? 'tombol-trx' : '' ;?>" href="<?= base_url('set_jadwal'); ?>">Setting Jadwal</a>
              <a class="collapse-item <?=($hal=="jadwal")?'active':'';?> <?= ($hal=="set_kelas")||($ku=="kelas/ubah") ||($ku=="jadwal/ubah")||($hal=="set_jadwal") ? 'tombol-trx' : '' ;?>" href="<?= base_url('jadwal'); ?>">Data Jadwal</a>
            </div>
          </div>
      </li>
      <?php endif; ?>
      <?php if ($user['akses'] == 1 || $user['akses'] == 2|| $user['akses'] == 3) : ?>
      <li class="nav-item <?= ($hal=="nilai_sikap_uts")||($hal=="nilai_sikap_uas") ? 'active' : '' ;?>">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapse4" aria-expanded="true" aria-controls="collapseThree">
          <i class="far fa-fw fas fa-child"></i>
          <span>Nilai Sikap</span></a>
          <div id="collapse4" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
              <a class="collapse-item <?=($hal=="nilai_sikap_uts")?'active':'';?> <?= ($hal=="set_kelas")||($ku=="kelas/ubah") ||($ku=="jadwal/ubah")||($hal=="set_jadwal")? 'tombol-trx' : '' ;?>" href="<?= base_url('nilai_sikap_uts'); ?>">Nilai Sikap UTS</a>
              <a class="collapse-item <?=($hal=="nilai_sikap_uas")?'active':'';?> <?= ($hal=="set_kelas")||($ku=="kelas/ubah") ||($ku=="jadwal/ubah")||($hal=="set_jadwal") ? 'tombol-trx' : '' ;?>" href="<?= base_url('nilai_sikap_uas'); ?>">Nilai Sikap UAS</a>
            </div>
          </div>
      </li>
      <li class="nav-item <?= ($hal=="nilai_kehadiran_uts")||($hal=="nilai_kehadiran_uas") ? 'active' : '' ;?>">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapse5" aria-expanded="true" aria-controls="collapseThree">
          <i class="far fa-fw fas fa-clock"></i>
          <span>Nilai Kehadiran</span></a>
          <div id="collapse5" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
              <a class="collapse-item <?=($hal=="nilai_kehadiran_uts")?'active':'';?> <?= ($hal=="set_kelas")||($ku=="kelas/ubah") ||($ku=="jadwal/ubah")||($hal=="set_jadwal")? 'tombol-trx' : '' ;?>" href="<?= base_url('nilai_kehadiran_uts'); ?>">Nilai Kehadiran UTS</a>
              <a class="collapse-item <?=($hal=="nilai_kehadiran_uas")?'active':'';?> <?= ($hal=="set_kelas")||($ku=="kelas/ubah") ||($ku=="jadwal/ubah")||($hal=="set_jadwal") ? 'tombol-trx' : '' ;?>" href="<?= base_url('nilai_kehadiran_uas'); ?>">Nilai Kehadiran UAS</a>
            </div>
          </div>
      </li>
      <li class="nav-item <?= ($hal=="catatan_uts")||($hal=="catatan_uas") ? 'active' : '' ;?>">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapse6" aria-expanded="true" aria-controls="collapseThree">
          <i class="far fa-fw fas fa-edit"></i>
          <span>Catatan Wali kelas</span></a>
          <div id="collapse6" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
              <a class="collapse-item <?=($hal=="catatan_uts")?'active':'';?> <?= ($hal=="set_kelas")||($ku=="kelas/ubah") ||($ku=="jadwal/ubah")||($hal=="set_jadwal")? 'tombol-trx' : '' ;?>" href="<?= base_url('catatan_uts'); ?>">Catatan Wali Kelas UTS</a>
              <a class="collapse-item <?=($hal=="catatan_uas")?'active':'';?> <?= ($hal=="set_kelas")||($ku=="kelas/ubah") ||($ku=="jadwal/ubah")||($hal=="set_jadwal") ? 'tombol-trx' : '' ;?>" href="<?= base_url('catatan_uas'); ?>">Catatan Wali Kelas UAS</a>
            </div>
          </div>
      </li>
      <li class="nav-item <?= ($hal=="ubah_password")? 'active' : '' ;?>">
        <a class="nav-link <?= ($hal=="set_kelas")||($ku=="kelas/ubah")||($ku=="jadwal/ubah")||($hal=="set_jadwal") ? 'tombol-trx' : '' ;?>" href="<?= base_url('ubah_password'); ?>">
          <i class="fas fa-fw fa-key"></i>
          <span>Ubah Password</span></a>
      </li>
      <?php endif; ?>
      <?php if ($user['akses'] == 1 ) : ?>
      <li class="nav-item <?= ($hal=="user")  ? 'active' : '' ;?>">
        <a class="nav-link <?= ($hal=="set_kelas")||($ku=="kelas/ubah") ||($ku=="jadwal/ubah")||($hal=="set_jadwal") ? 'tombol-trx' : '' ;?>" href="<?= base_url('user'); ?>">
          <i class="fas fa-fw fa-user-cog"></i>
          <span>Kelola User</span></a>
      </li>
      <?php endif; ?>

      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">
      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

          <!-- Sidebar Toggle (Topbar) -->
          <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button>

          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">
            <!-- Nav Item - User Information -->

              <li class="nav-item dropdown no-arrow">
                <a class="nav-link dropdown-toggle">
                  <span class="mr-2 d-none d-lg-inline text-gray-600 small"><?= $user['nama']; ?></span>
                  <img class="img-profile rounded-circle" src="<?= base_url(); ?>assets/images/user/<?= $user['avatar']; ?>">
                </a>
              </li>


            <div class="topbar-divider d-none d-sm-block"></div>
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link tombol-logout" href="<?= base_url('login/logout'); ?>">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small">Logout</span>
                <i class="fa fa-power-off"></i>
              </a>
            </li>
          </ul>

        </nav>
        <!-- End of Topbar -->
