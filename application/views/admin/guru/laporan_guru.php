<style>
	table{
		border-collapse: collapse;
	}
	table, th, td{
		border: solid black;
	}
	/* th, td{
		padding: 10px;
	} */
	th{
		background-color: rgb(19,110,170);
		color: white;
	}
	tr:hover{
		background-color: #f5f5f5;
	}
</style>
<center><h1><?= $sekolah['nama_sekolah'];?></h1></center>
<center><p><?= $sekolah['alamat'];?></p></center>
<center><p>Email : <?= $sekolah['email'];?>&nbsp; Website : <?= $sekolah['web'];?>&nbsp; Telepon : <?= $sekolah['telpon'];?></p></center>
<hr/>
<h3><center>Data Guru</center></h3>
<div class="table-responsive">
	<table border="1px"  width="100%" style="text-align:center;">
	<tr>
		<th>No</th>
		<th>NIP</th>
		<th>Nama Guru</th>
  	<th>E-Mail</th>
		<th>No Hp</th>
    <th>Alamat</th>
	</tr>
	<?php $no = 0; foreach($guru as $row) : $no++ ?>
		<tr>
			<td><?= $no; ?></td>
			<td><?= $row['nip']; ?></td>
			<td><?= $row['nama_guru']; ?></td>
			<td><?= $row['email']; ?></td>
			<td><?= $row['no_hp']; ?></td>
			<td><?= $row['alamat']; ?></td>
		</tr>
	<?php endforeach; ?>
</table>
</div>
