<div class="success-data" data-success="<?= $this->session->flashdata('success'); ?>"></div>
<div class="warning-data" data-warning="<?= $this->session->flashdata('warning'); ?>"></div>
<div class="error-data" data-error="<?= $this->session->flashdata('error'); ?>"></div>

<div class="container-fluid">
  <!-- /.card-header -->
  <section class="content">
      <div class="card">
          <div class="card-header">
              <h3 class="card-title">Tambah Guru</h3>
          </div>
            <div class="card-body table-responsive">
                <?php if (validation_errors()) : ?>
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <h5><i class="icon fas fa-ban"></i>Alert!</h5>
                        <?= validation_errors(); ?>
                    </div>
                <?php endif; ?>
                <?php echo form_open_multipart('guru/ubah'); ?>
                  <div class="card-body col-lg-6">
                    <div class="form-group">
                      <label for="exampleInputEmail1">NIP</label>
                        <input type="text" name="nip" class="form-control" value="<?= $view['nip'];?>">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Nama Guru</label>
                        <input type="text" name="nama_guru" class="form-control" value="<?= $view['nama_guru'];?>">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Email</label>
                        <input type="email" name="email" class="form-control" value="<?= $view['email'];?>">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">No Hp</label>
                        <input type="text" name="no_hp" class="form-control" value="<?= $view['no_hp'];?>">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputPassword1">Alamat</label>
                      <textarea name="alamat" class="textarea" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"><?= $view['alamat'];?></textarea>
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Foto</label>
                        <input type="file" name="gambar" class="form-control">
                    </div>
                    <input type="text" name="id_guru" value="<?= $view['id_guru'];?>" hidden>
                    <input type="text" name="gambar_lama" value="<?= $view['gambar'];?>" hidden>
                  <button type="submit" name="simpan" class="btn btn-primary">Simpan</button>
                </div>
          </div>
        </div>
      </section>
</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->
