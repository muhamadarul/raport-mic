<div class="success-data" data-success="<?= $this->session->flashdata('success'); ?>"></div>
<div class="warning-data" data-warning="<?= $this->session->flashdata('warning'); ?>"></div>
<div class="error-data" data-error="<?= $this->session->flashdata('error'); ?>"></div>

<div class="container-fluid">

  <!-- Page Heading -->
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Detail User &nbsp;<i class="fas fa-user-cog"></i></h1>
  </div>
        <!-- Table -->
              <div class="card shadow mb-4">
                <div class="card-header py-3">
                  <h6></h6><a  href="<?= base_url('user'); ?>" class="btn btn-primary btn-sm float-left"><i class="fa fa-sign-out-alt">&nbsp; Kembali</i></a>
                </div>
                <section class="content">
                  <div class="row">
                    <div class="col-sm-6">
                      <div class="card">
                        <ul class="list-group list-group-flush">
                          <li class="list-group-item">Nama          : &nbsp; <?= $admin["nama"]; ?> </li>
                          <li class="list-group-item">Username      : &nbsp; <?= $admin["username"]; ?> </li>
                            <?php if ($admin['aktif'] == 1) : ?>
                              <li class="list-group-item">Status : &nbsp;<span class="badge badge-success">Aktif</span></li>
                            <?php else : ?>
                              <li class="list-group-item">Status : &nbsp;<span class="badge badge-danger">Tidak Aktif</span></li>
                            <?php endif; ?>
                            <?php if ($admin['akses'] == 1) : ?>
                              <li class="list-group-item">Status : &nbsp;<span class="badge badge-danger">Administrator</span></li>
                            <?php elseif ($admin['akses'] == 2) : ?>
                              <li class="list-group-item">Status : &nbsp;<span class="badge badge-primary">Staff</span></li>
                            <?php else :?>
                              <li class="list-group-item">Status : &nbsp;<span class="badge badge-warning">Wali kelas</span></li>
                            <?php endif; ?>
                          <li class="list-group-item">Dibuat      : &nbsp; <?= $admin["dibuat"]; ?> </li>
                        </ul>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="row justify-content-center">
                          <ul class="list-group list-group-flush">
                              <li class="list-group-item"><img src="<?= base_url(); ?>assets/images/user/<?= $admin['avatar']; ?>" width="75%" title="<?= $admin['avatar']; ?>"></li>
                          </ul>
                      </div>
                    </div>
                  </div>
                </section>
              </div>
            </div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->
