<div class="success-data" data-success="<?= $this->session->flashdata('success'); ?>"></div>
<div class="warning-data" data-warning="<?= $this->session->flashdata('warning'); ?>"></div>
<div class="error-data" data-error="<?= $this->session->flashdata('error'); ?>"></div>

<div class="container-fluid">

  <!-- Page Heading -->
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Kelola User &nbsp;<i class="fas fa-user-cog"></i></h1>
  </div>
        <!-- Table -->
              <div class="card shadow mb-4">
                <div class="card-header py-3">
                  <h6></h6><a href="<?= base_url('user/tambah'); ?>" class="btn btn-primary btn-sm float-right"><i class="fa fa-plus">&nbsp; Tambah User</i></a>
                </div>
                <div class="card-body">
                  <div class="table-responsive">
                    <table class="table table-bordered" id="example1">
                      <thead>
                        <tr>
                          <th>No</th>
                          <th>Nama</th>
                          <th>Username</th>
                          <th>Aktif</th>
                          <th>Akses</th>
                          <th>Dibuat</th>
                          <th>Aksi</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php $no = 0; foreach($view as $row) : $no++ ?>
                        <tr>
                          <td><?= $no; ?></td>
                          <td><?= $row['nama']; ?></td>
                          <td><?= $row['username']; ?></td>
                          <td><?php if ($row['aktif'] == 1) : ?>
                              <span class="badge badge-success">Aktif</span>
                              <?php else : ?>
                              <span class="badge badge-danger">Tidak Aktif</span>
                              <?php endif; ?> </td>
                          <td><?php if ($row['akses'] == 1) : ?>
                              <span class="badge badge-danger">Administrator</span>
                              <?php elseif ($row['akses'] == 2) : ?>
                              <span class="badge badge-primary">Staff</span>
                              <?php else :?>
                              <span class="badge badge-warning">Wali kelas</span>
                              <?php endif; ?></td>
                          <td><?= $row['dibuat']; ?></td>
                          <td>
                            <a href="<?= base_url('user/ubah/'); ?><?= $row['id']; ?>" class="btn-circle btn-success btn-sm"><i class="fas fa-edit"></i></a>
                            <a href="<?= base_url('user/detail/'); ?><?= $row['id']; ?>" class="btn-circle btn-warning btn-sm"><i class="fas fa-eye"></i></a>
                            <a href="<?= base_url('user/hapus/'); ?><?= $row['id']; ?>" class="btn-circle btn-danger btn-sm tombol-hapus"><i class="fas fa-trash"></i></a>
                          </td>
                        </tr>
                      <?php endforeach; ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->
