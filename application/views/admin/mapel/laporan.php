<style>
	table{
		border-collapse: collapse;
	}
	table, th, td{
		border: solid black;
	}
	/* th, td{
		padding: 10px;
	} */
	th{
		background-color: rgb(19,110,170);
		color: white;
	}
	tr:hover{
		background-color: #f5f5f5;
	}
</style><center><h1><?= $sekolah['nama_sekolah'];?></h1></center>
<center><p><?= $sekolah['alamat'];?></p></center>
<center><p>Email : <?= $sekolah['email'];?>&nbsp; Website : <?= $sekolah['web'];?>&nbsp; Telepon : <?= $sekolah['telpon'];?></p></center>
<hr/>
<h2><center>Data Mata Pelajaran</center></h2>

	<table class="table table-bordered" border="1"  width="100%" style="text-align:center;">
	<tr>
		<th>No</th>
		<th>Muatan Lokal</th>
		<th>KKM</th>
		<th>Kelompok</th>
  </tr>
	<?php $no = 0; foreach($mapel as $row) : $no++ ?>
		<tr>
			<td><?= $no; ?></td>
			<td><?= $row['nama_mapel']; ?></td>
			<td><?= $row['kkm_mapel']; ?></td>
			<td><?= $row['nama_kelmapel']; ?></td>
		</tr>
	<?php endforeach; ?>
</table>
