<div class="container">
 <h3 align="center">Import Data Siswa</h3>
 <form method="post" id="import_form" enctype="multipart/form-data">
    <p><label>Pilih File Excel</label>
    <input type="file" name="file" id="file" required accept=".xls, .xlsx" /></p>
    <br />
    <input type="submit" id="import" name="import" value="Import" class="btn btn-info" />
 </form>
 <br/>
 <div class="table-responsive" id="_data">
 </div>
</div>

</div>
<!-- End of Main Content -->

<script type="text/javascript">

$(document).ready(function(){
 function load_data(){
   $.ajax({
     url:"<?= base_url('siswa/fetch');?>",
     method:"POST",
     success:function(data){
       $('#_data').html(data);
       console.log(data);
     }
   })
 }

 load_data();



 $('#import_form').on('submit', function(event){

   event.preventDefault();

   $.ajax({

     url:"<?= base_url('siswa/import');?>",

     method:"POST",

     data:new FormData(this),

     contentType:false,

     cache:false,

     processData:false,

     success:function(data){

       $('#file').val('');

       load_data();

     }

   })

 });

});

</script>
