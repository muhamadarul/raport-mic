<div class="success-data" data-success="<?= $this->session->flashdata('success'); ?>"></div>
<div class="warning-data" data-warning="<?= $this->session->flashdata('warning'); ?>"></div>
<div class="error-data" data-error="<?= $this->session->flashdata('error'); ?>"></div>

<div class="container-fluid">
  <!-- /.card-header -->
  <section class="content">
      <div class="card">
          <div class="card-header">
              <h3 class="card-title">Tambah Siswa</h3>
          </div>
            <div class="card-body table-responsive">
                <?php if (validation_errors()) : ?>
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <h5><i class="icon fas fa-ban"></i>Alert!</h5>
                        <?= validation_errors(); ?>
                    </div>
                <?php endif; ?>
                <?php echo form_open_multipart('siswa/tambah'); ?>
                  <div class="card-body">
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="exampleInputEmail1">NIS <span class="text-danger">*</span></label>
                            <input type="text" name="nis" class="form-control" required>
                        </div>
                        <div class="form-group">
                          <label for="exampleInputEmail1">NISN <span class="text-danger">*</span></label>
                            <input type="text" name="nisn" class="form-control" required>
                        </div>
                        <div class="form-group">
                          <label for="exampleInputEmail1">Nama Siswa <span class="text-danger">*</span></label>
                            <input type="text" name="nama_siswa" class="form-control" required>
                        </div>
                        <div class="form-group">
                          <label for="exampleInputEmail1">Tempat Lahir <span class="text-danger">*</span></label>
                            <input type="text" name="tmp_lahir" class="form-control" required>
                        </div>
                        <div class="form-group">
                          <label for="exampleInputEmail1">Tgl Lahir <span class="text-danger">*</span></label>
                            <input type="date" name="tgl_lahir" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label>Jenis Kelamin</label>
                            <select name="jk" class="form-control id_akses" style="width: 100%;">
                              <option value="">-- Pilih Jenis Kelamin --</option>
                              <option value="Laki-laki">Laki-laki</option>
                              <option value="Perempuan">Perempuan</option>
                            </select>
                        </div>
                        <div class="form-group">
                          <label for="exampleInputEmail1">Alamat</label>
                            <textarea name="alamat" class="form-control" rows="8" cols="80"></textarea>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="exampleInputEmail1">Anak Ke- </label>
                            <input type="number" name="anakke" class="form-control">
                        </div>
                        <div class="form-group">
                          <label for="exampleInputEmail1">Asal Sekolah <span class="text-danger">*</span></label>
                            <input type="text" name="asal_sek" class="form-control" required>
                        </div>
                        <div class="form-group">
                          <label for="exampleInputEmail1">Nama Ayah <span class="text-danger">*</span></label>
                            <input type="text" name="ortu_ayah" class="form-control" required>
                        </div>
                        <div class="form-group">
                          <label for="exampleInputEmail1">Nama Ibu <span class="text-danger">*</span></label>
                            <input type="text" name="ortu_ibu" class="form-control" required>
                        </div>
                        <div class="form-group">
                          <label for="exampleInputEmail1">Alamat Ortu <span class="text-danger">*</span></label>
                            <textarea name="ortu_alamat" class="form-control" rows="8" cols="80"></textarea>
                        </div>
                        <div class="form-group">
                          <label for="exampleInputEmail1">Telepon Orang Tua <span class="text-danger">*</span></label>
                            <input type="text" name="ortu_telp" class="form-control" required>
                        </div>
                        <div class="form-group">
                          <label for="exampleInputEmail1">Email Orang Tua <span class="text-danger">*</span></label>
                            <input type="email" name="ortu_email" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputFile">Foto Siswa</label>
                              <input name="foto_siswa" type="file" class="form-control">
                        </div>
                      </div>
                    </div>



                  <button type="submit" name="simpan" class="btn btn-primary">Simpan</button>
                </div>
          </div>
        </div>
      </section>
</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->
