<div class="success-data" data-success="<?= $this->session->flashdata('success'); ?>"></div>
<div class="warning-data" data-warning="<?= $this->session->flashdata('warning'); ?>"></div>
<div class="error-data" data-error="<?= $this->session->flashdata('error'); ?>"></div>

<div class="container-fluid">

  <!-- Page Heading -->
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Detail Siswa &nbsp;<i class="fas fa-user"></i></h1>
  </div>
        <!-- Table -->
              <div class="card shadow mb-4">
                <div class="card-header py-3">
                  <h6></h6><a  href="<?= base_url('siswa'); ?>" class="btn btn-primary btn-sm float-left"><i class="fa fa-sign-out-alt">&nbsp; Kembali</i></a>
                </div>
                <section class="content">
                  <div class="row">
                    <div class="col-sm-6">
                      <div class="card">
                        <ul class="list-group list-group-flush">
                          <li class="list-group-item">NIS       : &nbsp; <?= $view["nis"]; ?> </li>
                          <li class="list-group-item">NISN       : &nbsp; <?= $view["nisn"]; ?> </li>
                          <li class="list-group-item">Nama Siswa        : &nbsp; <?= $view["nama_siswa"]; ?> </li>
                          <li class="list-group-item">Jenis Kelamin      : &nbsp; <?= $view["jk"]; ?> </li>
                          <li class="list-group-item">Tempat, Tgl Lahir    : &nbsp;<?= $view['tmp_lahir']; ?>, <?= date('d F Y', strtotime($view['tgl_lahir'])) ?> </li>
                          <li class="list-group-item">Anak ke       : &nbsp; <?= $view["anakke"]; ?> </li>
                          <li class="list-group-item">Alamat     : &nbsp; <?= $view["alamat"]; ?> </li>
                          <li class="list-group-item">Asal Sekolah   : &nbsp; <?= $view["asal_sek"]; ?> </li>
                          <li class="list-group-item">Ortu Ayah     : &nbsp; <?= $view["ortu_ayah"]; ?> </li>
                          <li class="list-group-item">Ortu Ibu     : &nbsp; <?= $view["ortu_ibu"]; ?> </li>
                          <li class="list-group-item">Alamat Ortu     : &nbsp; <?= $view["ortu_alamat"]; ?> </li>
                          <li class="list-group-item">Telepon Ortu    : &nbsp; <?= $view["ortu_telp"]; ?> </li>
                          <li class="list-group-item">Email Ortu    : &nbsp; <?= $view["ortu_email"]; ?> </li>
                          <li class="list-group-item">Dibuat    : &nbsp; <?= $view["dibuat"]; ?> </li>
                        </ul>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="row justify-content-center">
                          <ul class="list-group list-group-flush">
                              <li class="list-group-item"><img src="<?= base_url(); ?>assets/images/siswa/<?= $view['foto_siswa']; ?>" width="75%" title="<?= $view['foto_siswa']; ?>"></li>
                          </ul>
                      </div>
                    </div>
                  </div>
                </section>
              </div>
            </div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->
