<div class="success-data" data-success="<?= $this->session->flashdata('success'); ?>"></div>
<div class="warning-data" data-warning="<?= $this->session->flashdata('warning'); ?>"></div>
<div class="error-data" data-error="<?= $this->session->flashdata('error'); ?>"></div>

<div class="container-fluid">

  <!-- Page Heading -->
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Data Siswa &nbsp;<i class="fas fa-users"></i></h1>
      <a href="<?= base_url('siswa/cetak'); ?>" target="_blank" class="d-none d-sm-inline-block btn btn-md btn-danger shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report PDF</a>
  </div>
        <!-- Table -->
              <div class="card shadow mb-4">
                <div class="card-header py-3">
                  <a href="<?= base_url('siswa/tambah'); ?>" class="btn btn-primary btn-sm float-right"><i class="fa fa-plus">&nbsp; Tambah Siswa</i></a>
                  <a href="<?= base_url('siswa/import_siswa'); ?>" class="btn btn-success btn-sm float-left"><i class="fa fa-file">&nbsp; Import Siswa</i></a>
                </div>
                <div class="card-body">
                  <div class="table-responsive">
                    <table class="table table-bordered" id="example1">
                      <thead>
                        <tr>
                          <th>No</th>
                          <th>NIS</th>
                          <th>Nama</th>
                          <th>Jenis Kelamin</th>
                          <th>Tempat, Tgl Lahir</th>
                          <th>Alamat</th>
                          <th>Aksi</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php $no = 0; foreach($view as $row) : $no++ ?>
                        <tr>
                          <td><?= $no; ?></td>
                          <td><?= $row['nis']; ?></td>
                          <td><?= $row['nama_siswa']; ?></td>
                          <td><?= $row['jk']; ?></td>
                          <td><?= $row['tmp_lahir']; ?>, <?= date('d F Y', strtotime($row['tgl_lahir'])) ?></td>
                          <td><?= substr($row['alamat'], 0, 50); ?></td>
                          <td>
                            <a href="<?= base_url('siswa/ubah/'); ?><?= $row['id_siswa']; ?>" class="btn-circle btn-success btn-sm"><i class="fas fa-edit"></i></a>
                            <a href="<?= base_url('siswa/detail/'); ?><?= $row['id_siswa']; ?>" class="btn-circle btn-warning btn-sm"><i class="fas fa-eye"></i></a>
                            <?php if ($user['akses']==1) :?>
                            <a href="<?= base_url('siswa/hapus/'); ?><?= $row['id_siswa']; ?>" class="btn-circle btn-danger btn-sm tombol-hapus"><i class="fas fa-trash"></i></a>
                            <?php endif;?>
                          </td>
                        </tr>
                      <?php endforeach; ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->
