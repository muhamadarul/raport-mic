<style>
	table{
		border-collapse: collapse;
	}
	table, th, td{
		border: solid black;
	}
	/* th, td{
		padding: 10px;
	} */
	th{
		background-color: rgb(19,110,170);
		color: white;
	}
	tr:hover{
		background-color: #f5f5f5;
	}
</style>
<center><h1><?= $sekolah['nama_sekolah'];?></h1></center>
<center><p><?= $sekolah['alamat'];?></p></center>
<center><p>Email : <?= $sekolah['email'];?>&nbsp; Website : <?= $sekolah['web'];?>&nbsp; Telepon : <?= $sekolah['telpon'];?></p></center>
<hr/>
<h3><center>Data Siswa</center></h3>
	<table border="1px"  width="100%" style="text-align:center;">
	<tr>
		<th>No</th>
		<th>NIS</th>
		<th>NISN</th>
  	<th>Nama</th>
		<th>JK</th>
    <th>Tempat, Tgl Lahir</th>
    <th>Anak Ke</th>
    <th>Alamat</th>
    <th>Asal Sekolah</th>
    <th>Ayah</th>
    <th>Ibu</th>
    <th>Alamat</th>
    <th>Telp</th>
    <th>Email</th>
	</tr>
	<?php $no = 0; foreach($siswa as $row) : $no++ ?>
		<tr>
			<td><?= $no; ?></td>
			<td><?= $row['nis']; ?></td>
			<td><?= $row['nisn']; ?></td>
			<td><?= $row['nama_siswa']; ?></td>
			<td><?= $row['jk']; ?></td>
			<td><?= $row['tmp_lahir']; ?>, <?= date('d F Y', strtotime($row['tgl_lahir'])) ?></td>
      <td><?= $row['anakke']; ?></td>
      <td><?= $row['alamat']; ?></td>
      <td><?= $row['asal_sek']; ?></td>
      <td><?= $row['ortu_ayah']; ?></td>
      <td><?= $row['ortu_ibu']; ?></td>
      <td><?= $row['ortu_alamat']; ?></td>
      <td><?= $row['ortu_telp']; ?></td>
      <td><?= $row['ortu_email']; ?></td>

		</tr>
	<?php endforeach; ?>
</table>
