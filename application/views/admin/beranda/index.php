<div class="success-data" data-success="<?= $this->session->flashdata('success'); ?>"></div>
<div class="warning-data" data-warning="<?= $this->session->flashdata('warning'); ?>"></div>
<div class="error-data" data-error="<?= $this->session->flashdata('error'); ?>"></div>

<div class="container-fluid">

  <!-- Page Heading -->
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
  </div>

  <!-- Content Row -->
  <div class="row">
    <!-- Komentar Card Example -->
    <div class="col-xl-2 col-md-2 mb-4">
      <div class="card border-left-danger shadow h-100 py-2">
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              <div class="text-xs font-weight-bold text-danger text-uppercase mb-1">Kelas</div>
              <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $hitungkelas; ?></div>
            </div>
            <div class="col-auto">
              <a href="<?= base_url('kelas');?>"><i class="fas fa-school fa-2x text-gray-300"></i></a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Artikel Card Example -->
    <div class="col-xl-2 col-md-2 mb-4">
      <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Mata Pelajaran</div>
              <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $hitungmapel; ?></div>
            </div>
            <div class="col-auto">
              <a href="<?= base_url('mulok');?>"><i class="fas fa-file fa-2x text-gray-300"></i></a>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Komentar Card Example -->
    <div class="col-xl-2 col-md-2 mb-4">
      <div class="card border-left-success shadow h-100 py-2">
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Ekstrakurikuler</div>
              <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $hitungeks; ?></div>
            </div>
            <div class="col-auto">
              <a href="<?= base_url('ekstrakurikuler');?>"><i class="fas fa-running fa-2x text-gray-300"></i></a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Siswa Card Example -->
    <div class="col-xl-2 col-md-2 mb-4">
      <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Siswa</div>
              <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $hitungsiswa; ?></div>
            </div>
            <div class="col-auto">
              <a href="<?= base_url('siswa');?>"><i class="fas fa-users fa-2x text-gray-300"></i></a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Guru Card Example -->
    <div class="col-xl-2 col-md-2 mb-4">
      <div class="card border-left-success shadow h-100 py-2">
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Guru</div>
              <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $hitungguru; ?></div>
            </div>
            <div class="col-auto">
              <a href="<?= base_url('guru');?>"><i class="fas fa-users fa-2x text-gray-300"></i></a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Komentar Card Example -->
    <div class="col-xl-2 col-md-2 mb-4">
      <div class="card border-left-warning shadow h-100 py-2">
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              <div class="text-xs font-weight-bold text-success text-uppercase mb-1">User</div>
              <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $hitunguser; ?></div>
            </div>
            <div class="col-auto">
              <a href="<?= base_url('user');?>"><i class="fas fa-users-cog fa-2x text-gray-300"></i></a>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>
  <div class="row">
    <div class="col-md-6">
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary">Visi & Misi Sekolah</h6>
        </div>
        <div class="card-body">
          <div class="text-center">
            <img class="img-fluid px-3 px-sm-4 mt-3 mb-4" style="width: 25rem;" src="<?= base_url(); ?>assets/images/beranda/undraw_posting_photo.svg" alt="">
          </div>
          <h3>Visi</h3>
          <p><?= $sekolah['visi'];?></p>
          <h3>Misi</h3>
          <p><?= $sekolah['misi'];?></p>
        </div>
      </div>
    </div>
    <div class="col-md-6">
      <!-- Project Card Example -->
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary">Profil Sekolah</h6>
        </div>
        <div class="card-body">
          <h4 class="small font-weight-bold">Nama Sekolah</h4>
          <p><?= $sekolah['nama_sekolah'];?></p>
          <h4 class="small font-weight-bold">Kepala Sekolah</h4>
          <p><?= $sekolah['kepsek'];?></p>
          <h4 class="small font-weight-bold">Email Sekolah</h4>
          <p><?= $sekolah['email'];?></p>
          <h4 class="small font-weight-bold">Web Sekolah</h4>
          <p><?= $sekolah['web'];?></p>
          <h4 class="small font-weight-bold">Telepon Sekolah</h4>
          <p><?= $sekolah['telpon'];?></p>
          <h4 class="small font-weight-bold">Alamat Sekolah</h4>
          <p><?= $sekolah['alamat'];?></p>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- /.container-fluid -->
</div>
<!-- End of Main Content -->
