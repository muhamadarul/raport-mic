<div class="success-data" data-success="<?= $this->session->flashdata('success'); ?>"></div>
<div class="warning-data" data-warning="<?= $this->session->flashdata('warning'); ?>"></div>
<div class="error-data" data-error="<?= $this->session->flashdata('error'); ?>"></div>

<div class="container-fluid">
  <?php
  $id_kelas= $this->uri->segment(3);
   ?>
  <!-- Page Heading -->
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Detail Kelas &nbsp;<i class="fas fa-users"></i></h1>
    <a href="<?= base_url('kelas/cetak/'); ?><?= $id_kelas; ?>" target="_blank" class="d-none d-sm-inline-block btn btn-md btn-danger shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report PDF</a>
  </div>

                <section class="content">
                  <div class="row">
                    <div class="col-md-6">
                      <div class="card">
                        <ul class="list-group list-group-flush">
                          <li class="list-group-item">Tahun Ajaran       : &nbsp; <?= $view["thn_ajaran"]; ?> </li>
                          <li class="list-group-item">Nama Kelas       : &nbsp; <?= $view["nama_kelas"]; ?> </li>
                          <li class="list-group-item">Wali Kelas        : &nbsp; <?= $view["nama_guru"]; ?> </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </section>
                    <br>
                    <!-- Table -->
                  <div class="card shadow mb-4">
                    <div class="row">
                      <div class="col-md-12">
                        <div class="card-body">
                          <div class="table-responsive">
                            <table class="table table-bordered" id="example1">
                              <thead>
                                <tr>
                                  <th>NO</th>
                                  <th>NIS</th>
                                  <th>Nama</th>
                                  <th>Jenis Kelamin</th>
                                  <th>Tempat, Tanggal Lahir</th>
                                </tr>
                              </thead>
                              <tbody>
                              <?php $no = 0; foreach($siswa as $row) : $no++ ?>
                                <tr>
                                  <td><?= $no; ?></td>
                                  <td><?= $row['nis']; ?></td>
                                  <td><?= $row['nama_siswa']; ?></td>
                                  <td><?= $row['jk']; ?></td>
                                  <td><?= $row['tmp_lahir']; ?>, <?= date('d F Y', strtotime($row['tgl_lahir'])) ?></td>
                                </tr>
                              <?php endforeach; ?>
                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                    <a  href="<?= base_url('kelas'); ?>" class="btn btn-primary btn-sm float-left"><i class="fa fa-sign-out-alt">&nbsp; Kembali</i></a>

                </div>




<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->
