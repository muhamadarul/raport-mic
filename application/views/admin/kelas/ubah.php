<div class="success-data" data-success="<?= $this->session->flashdata('success'); ?>"></div>
<div class="warning-data" data-warning="<?= $this->session->flashdata('warning'); ?>"></div>
<div class="error-data" data-error="<?= $this->session->flashdata('error'); ?>"></div>

<div class="container-fluid">

  <!-- Page Heading -->
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Ubah Kelas Siswa &nbsp;<i class="fas fa-users"></i></h1>
  </div>

      <div class="card-body">
          <div class="row">
            <div class="col-md-4">
              <center><font color="red"><p id="pesan"></p></font></center>
              <div class="form-group">
                <label for="exampleInputEmail1">Nama Kelas  <span class="text-danger">*</span></label>
                  <input type="text" class="form-control" name="nama_kelas" id="nm_kelas" value="<?= $view['nama_kelas']; ?>">
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Pilih Tahun Ajaran  <span class="text-danger">*</span></label>
                  <select name="tahun_ajaran" class="form-control" id="ta" style="width: 100%;">
                      <option value="">-- Pilih Tahun Ajaran --</option>
                      <?php foreach ($tahun as $row) : ?>
                          <option value="<?= $row['id_ta']; ?>" <?= $view['tahun_ajaran'] == $row['id_ta'] ? 'selected' : ''?>><?= $row['thn_ajaran']; ?></option>
                      <?php endforeach; ?>
                  </select>
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Pilih Wali Kelas  <span class="text-danger">*</span></label>
                  <select name="wali_kelas" class="form-control" id="wali" style="width: 100%;">
                      <option value="">-- Pilih Wali Kelas --</option>
                      <?php foreach ($guru as $row) : ?>
                          <option value="<?= $row['id_guru']; ?>" <?= $view['wali_kelas'] == $row['id_guru'] ? 'selected' : ''?>><?= $row['nama_guru']; ?></option>
                      <?php endforeach; ?>
                  </select>
              </div>
                <input type="text" class="form-control" name="id_kelas"  value="<?= $view['id_kelas']; ?>" hidden>
              <button type="submit" id="addKelas" onclick="ubahKelas()" class="btn btn-primary">UBAH</button>
            </div>
            <div class="col-md-5">
              <center><font color="red"><p id="pesan2"></p></font></center>
              <div class="form-group">
                <!-- <label for="exampleInputEmail1">id_kelas <span class="text-danger">*</span></label> -->
                  <input type="text" class="form-control" name="kelas" id="kelas" value="<?= $view['kelas']; ?>" hidden>
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Pilih Siswa  <span class="text-danger">*</span></label>
                  <select class="itemSiswa form-control" name="itemSiswa" style="width: 100%;">
                  </select>
              </div>
              <button type="submit" name="pilih" onclick="tambahSiswa()" class="btn btn-secondary">Tambah</button>
            </div>
          </div>
        </div>
        <!-- Table -->
              <div class="card shadow mb-4">
                <div class="card-body">
                  <div class="table-responsive">
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th>NO</th>
                          <!-- <th>ID</th> -->
                          <th>Kelas</th>
                          <th>Nama</th>
                          <th>NIS</th>
                          <th>Aksi</th>
                        </tr>
                      </thead>
                      <tbody id="target">

                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
              <a id="konfirmasi" href="<?= base_url('kelas'); ?>" class="btn btn-primary konfirmasi ">SIMPAN DATA KELAS &nbsp;<i class="fas fa-users"></i></a><br>
            </div>
<!-- /.container-fluid -->
</div>
<!-- End of Main Content -->

<script type="text/javascript">
  $(document).ready(function(){
    tampilSiswa();
  });



  $('.itemSiswa').select2({
    ajax:{
      url : '<?= base_url('set_kelas/ambil_siswa'); ?>',
      dataType:"json",
      delay:250,
      data:function(params){
        return {
          siswa : params.term
        };
      },
      processResults:function(data){
        var results=[];
        $.each(data, function(index,item){
          results.push({
            id : item.id_siswa,
            text : item.nama_siswa
          });
        });
        return {
          results:results
        };
      }
    }
  });

  function ubahKelas() {
    var id_kelas = $("[name = 'id_kelas']").val();
    var nama_kelas = $("[name = 'nama_kelas']").val();
    var tahun_ajaran = $("[name = 'tahun_ajaran']").val();
    var wali_kelas = $("[name = 'wali_kelas']").val();
      $.ajax({
        type:'POST',
        data:'id_kelas='+id_kelas+'&nama_kelas='+nama_kelas+'&tahun_ajaran='+tahun_ajaran+'&wali_kelas='+wali_kelas,
        url :'<?= base_url().'kelas/ubahkelas'?>',
        chace : false,
        dataType:'json',
          success:function(hasil){
            console.log(hasil);
            $('#pesan').html(hasil.pesan);
            if (hasil.pesan=='') {
              Swal.fire({
                  title: 'Berhasil ',
                  text: 'Data Berhasil Diubah!',
                  type: 'success'
              });
                tampilSiswa();
            }
          }
      });
  }

  function tambahSiswa() {
    var kelas = $("[name = 'kelas']").val();
    var itemSiswa = $("[name = 'itemSiswa']").val();
      $.ajax({
        type:'POST',
        data:'kelas='+kelas+'&itemSiswa='+itemSiswa,
        chace : false,
        url :'<?= base_url().'set_kelas/tambahsiswa'?>',
        dataType:'json',
          success:function(hasil){
            console.log(hasil);
            $('#pesan2').html(hasil.pesan);
            if (hasil.pesan=='') {
              Swal.fire({
                  title: 'Berhasil ',
                  text: 'Siswa Telah Ditambahkan!',
                  type: 'success'
              });
              tampilSiswa()
            }

        }
      });
  }
  function tampilSiswa(){
      var id = $("[name = 'kelas']").val();
    $.ajax({
        type:'POST',
        data:'id='+id,
        url :'<?= base_url().'set_kelas/tampilsiswa_edit'?>',
        dataType:'json',
          success:function(data){
            var baris ='';
            var n='';

            for(var i=0;i<data.length;i++){
              n=i+1;
              baris += '<tr>'+
                            '<td>'+  n +'</td>'+
                            // '<td>'+ data[i].id_kelas_detail+'</td>'+
                            '<td>'+ data[i].nama_kelas+'</td>'+
                            '<td>'+ data[i].nama_siswa+'</td>'+
                            '<td>'+ data[i].nis+'</td>'+
                            '<td><a onclick="hapusSiswa('+ data[i].id_kelas_detail+')" class="btn-circle btn-danger btn-sm hapus"><i class="fas fa-trash"></i></a></td>'+
                       '</tr>';
            }
            $('#target').html(baris);

          }
      });
  }



  function hapusSiswa(id) {
    Swal.fire({
        title: "Apakah anda yakin?",
        text: "data akan dihapus!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya, Hapus Data!'
    }).then((result) => {
        if (result.value) {
          $.ajax({
            type:'POST',
            data:'id_kelas_detail='+id,
            url:'<?= base_url().'set_kelas/hapussiswa'?>',
            success : function() {
                tampilSiswa();
            }
          });
        }
    });

  }



</script>
