<style>
	table{
		border-collapse: collapse;
	}
	table, th, td{
		border: solid black;
	}
	th, td{
		padding: 10px;
	}
	th{
		background-color: rgb(19,110,170);
		color: white;
	}
	tr:hover{
		background-color: #f5f5f5;
	}
</style>
<center><h1><?= $sekolah['nama_sekolah'];?></h1></center>
<center><p><?= $sekolah['alamat'];?></p></center>
<center><p>Email : <?= $sekolah['email'];?>&nbsp; Website : <?= $sekolah['web'];?>&nbsp; Telepon : <?= $sekolah['telpon'];?></p></center>
<hr/>
<h3><center>Data Kelas</center></h3>
<div class="table-responsive">
	<table class="table table-bordered" border=""  width="50%" style="text-align:left;">
	<tr>
		<td>Tahun Ajaran</td>
    <td>:</td>
    <td><?= $view["thn_ajaran"]; ?></td>
  </tr>
  <tr>
    <td>Nama Kelas</td>
    <td>:</td>
    <td><?= $view["nama_kelas"]; ?></td>
  </tr>
  <tr>
    <td>Wali Kelas</td>
    <td>:</td>
    <td><?= $view["nama_guru"]; ?></td>
  </tr>
</table>
</div>
<!-- Table -->
  <table border="1px"  width="100%" style="text-align:center;">
      <thead>
        <tr>
          <th>NO</th>
          <th>NIS</th>
          <th>Nama</th>
          <th>Jenis Kelamin</th>
          <th>Tempat, Tanggal Lahir</th>
        </tr>
      </thead>
      <tbody>
      <?php $no = 0; foreach($siswa as $row) : $no++ ?>
        <tr>
          <td><?= $no; ?></td>
          <td><?= $row['nis']; ?></td>
          <td><?= $row['nama_siswa']; ?></td>
          <td><?= $row['jk']; ?></td>
          <td><?= $row['tmp_lahir']; ?>, <?= date('d F Y', strtotime($row['tgl_lahir'])) ?></td>
        </tr>
      <?php endforeach; ?>
      </tbody>
    </table>
