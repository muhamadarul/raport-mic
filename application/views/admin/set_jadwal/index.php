<div class="success-data" data-success="<?= $this->session->flashdata('success'); ?>"></div>
<div class="warning-data" data-warning="<?= $this->session->flashdata('warning'); ?>"></div>
<div class="error-data" data-error="<?= $this->session->flashdata('error'); ?>"></div>

<div class="container-fluid">

  <!-- Page Heading -->
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Setting Jadwal Mapel &nbsp;<i class="fas fa-pen"></i></h1>
  </div>

      <div class="card-body">
          <div class="row">
            <div class="col-md-4">
              <center><font color="red"><p id="pesan"></p></font></center>
              <div class="form-group">
                <label for="exampleInputEmail1">Pilih Kelas  <span class="text-danger">*</span></label>
                  <select name="kelas" class="form-control" id="kelas" style="width: 100%;">
                      <option value="">-- Pilih Kelas --</option>
                      <?php foreach ($kelas as $row) : ?>
                          <option value="<?= $row['id_kelas']; ?>"><?= $row['nama_kelas']; ?>&nbsp; <?= $row['thn_ajaran']?></option>
                      <?php endforeach; ?>
                  </select>
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Nama Jadwal</label>
                  <input type="text" name="nama_jadwal" id="nama_jadwal" class="form-control">
              </div>
              <button type="submit" id="addJadwal" onclick="tambahJadwal()" class="btn btn-primary">Simpan</button>
            </div>
            <div class="col-md-5">
              <center><font color="red"><p id="pesan2"></p></font></center>
              <div class="form-group">
                <!-- <label for="exampleInputEmail1">id_kelas <span class="text-danger">*</span></label> -->
                  <input type="text" class="form-control" name="jadwal" id="jadwal" value="" hidden>
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Pilih Mata Pelajaran  <span class="text-danger">*</span></label>
                  <select class="itemMapel form-control" name="itemMapel" id="itemMapel" style="width: 100%;">
                  </select>
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Pilih Hari  <span class="text-danger">*</span></label>
                  <select class="form-control" name="hari" id="hari" style="width: 100%;">
                    <option value="">-- Pilih Hari --</option>
                    <?php foreach ($hari as $row) : ?>
                        <option value="<?= $row['id_hari']; ?>"><?= $row['nama_hari']; ?></option>
                    <?php endforeach; ?>
                  </select>
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Jam Belajar</label>
                  <input type="text" name="jam" id="jam" class="form-control">
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Pilih Pengampu  <span class="text-danger">*</span></label>
                  <select class="form-control" name="pengampu" id="pengampu" style="width: 100%;">
                    <option value="">-- Pilih Pengampu --</option>
                    <?php foreach ($guru as $row) : ?>
                        <option value="<?= $row['id_guru']; ?>"><?= $row['nama_guru']; ?></option>
                    <?php endforeach; ?>
                  </select>
              </div>
              <button type="submit" name="pilih" onclick="tambahJadwalDetail()" class="btn btn-secondary">Tambah</button>
            </div>
          </div>
        </div>
        <!-- Table -->
              <div class="card shadow mb-4">
                <div class="card-body">
                  <div class="table-responsive">
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th>NO</th>
                          <th>Kelas</th>
                          <th>Mapel</th>
                          <th>Hari</th>
                          <th>Jam</th>
                          <th>Pengampu</th>
                          <th>Aksi</th>
                        </tr>
                      </thead>
                      <tbody id="target">

                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
              <a id="konfirmasi" href="<?= base_url('jadwal'); ?>" class="btn btn-primary konfirmasi ">SIMPAN DATA JADWAL &nbsp;<i class="fas fa-pen"></i></a><br>
            </div>
<!-- /.container-fluid -->
</div>
<!-- End of Main Content -->

          <!-- MODAL EDIT JADWAL DETAIL   -->
                <div class="modal fade" id="editjadwal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Edit Detail Jadwal</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                        <center><font color="red"><p id="pesan3"></p></font></center>
                        <form id="formEdit">
                          <div class="form-group" hidden>
                            <label for="exampleInputEmail1">Pilih Mata Pelajaran  <span class="text-danger">*</span></label>
                              <select class="itemMapel2 form-control" name="itemMapel2" style="width: 100%;" selected="selected">
                              </select>
                          </div>
                          <div class="form-group">
                            <label for="exampleInputEmail1">Pilih Hari  <span class="text-danger">*</span></label>
                              <select class="form-control" name="hari2" style="width: 100%;">
                                <option value="">-- Pilih Hari --</option>
                                <?php foreach ($hari as $row) : ?>
                                    <option value="<?= $row['id_hari']; ?>"><?= $row['nama_hari']; ?></option>
                                <?php endforeach; ?>
                              </select>
                          </div>
                          <div class="form-group">
                            <label for="exampleInputEmail1">Jam Belajar</label>
                              <input type="text" name="jam2" class="form-control">
                          </div>
                          <div class="form-group">
                            <label for="exampleInputEmail1">Pilih Pengampu  <span class="text-danger">*</span></label>
                              <select class="form-control" name="pengampu2" style="width: 100%;">
                                <option value="">-- Pilih Pengampu --</option>
                                <?php foreach ($guru as $row) : ?>
                                    <option value="<?= $row['id_guru']; ?>"><?= $row['nama_guru']; ?></option>
                                <?php endforeach; ?>
                              </select>
                          </div>
                          <input type="text" name="id_jdetail" value="" hidden>
                        </form>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" id="btnModal" onclick="ubahJadwalDetail()" >Simpan</button>
                      </div>
                    </div>
                  </div>
                </div>

<script type="text/javascript">
// $(document).ready(function() {
// $('.itemMapel2').select2()
// });
  $('.itemMapel').select2({
    ajax:{
      url : '<?= base_url('set_jadwal/ambilmapel'); ?>',
      dataType:"json",
      delay:250,
      data:function(params){
        return {
          mapel : params.term
        };
      },
      processResults:function(data){
        var results=[];
        $.each(data, function(index,item){
          results.push({
            id : item.id_mapel,
            text : item.nama_mapel
          });
        });
        return {
          results:results
        };
      }
    }
  });
  $('.itemMapel2').select2({
    ajax:{
      url : '<?= base_url('set_jadwal/ambilmapel'); ?>',
      dataType:"json",
      delay:250,
      data:function(params){
        return {
          mapel : params.term
        };
      },
      processResults:function(data){
        var results=[];
        $.each(data, function(index,item){
          results.push({
            id : item.id_mapel,
            text : item.nama_mapel
          });
        });
        return {
          results:results
        };
      }
    }
  });
  function tambahJadwalDetail() {
    var jadwal = $("[name = 'jadwal']").val();
    var itemMapel = $("[name = 'itemMapel']").val();
    var hari = $("[name = 'hari']").val();
    var jam = $("[name = 'jam']").val();
    var pengampu = $("[name = 'pengampu']").val();
      $.ajax({
        type:'POST',
        data:'jadwal='+jadwal+'&itemMapel='+itemMapel+'&hari='+hari+'&jam='+jam+'&pengampu='+pengampu,
        chace : false,
        url :'<?= base_url().'set_jadwal/tambahjadwaldetail'?>',
        dataType:'json',
          success:function(hasil){
            $('#pesan2').html(hasil.pesan);
            if (hasil.pesan=='') {
              tampilJadwalDetail();
              // document.getElementById('itemMapel').value="";
              document.getElementById('hari').value="";
              document.getElementById('jam').value="";
              document.getElementById('pengampu').value="";
              Swal.fire({
                  title: 'Berhasil ',
                  text: 'Mata Pelajaran Telah Ditambahkan Pada Jadwal Ini!',
                  type: 'success'
              });
            }
        }
      });
  }
  function JadwalDetail(id) {
    $.ajax({
      type:"POST",
      data:'id='+id,
      url:'<?= base_url().'set_jadwal/ambil_id_detail'?>',
      dataType:'json',
      success:function (hasil) {
        console.log(hasil);
        // $('.itemMapel2').val(hasil.mapel);
        $("[name = 'hari2']").val(hasil.hari);
        $("[name = 'jam2']").val(hasil.jam);
        $("[name = 'pengampu2']").val(hasil.pengampu);
        $("[name = 'id_jdetail']").val(hasil.id_jadwal_detail);
        //
        // var id_m = hasil.mapel;
        //     $.ajax({
        //       dataType:"json",
        //       data:'id_m='+id_m,
        //       delay:250,
        //       url : '<= base_url('set_jadwal/ambilmapel'); ?>',
        //        success: function(data){
        //          $.each(data, function(index,item){
        //            $('.itemMapel2').select2("trigger", "select", {
        //            data: { id: hasil.mapel }
        //            });
        //          });
        //       }
        //     });
      }
    });
  }
  function ubahJadwalDetail() {
    // var jadwal = $("[name = 'jadwal']").val();
    var id_jdetail = $("[name = 'id_jdetail']").val();
    // var itemMapel2 = $("[name = 'itemMapel2']").val();
    var hari2 = $("[name = 'hari2']").val();
    var jam2 = $("[name = 'jam2']").val();
    var pengampu2 = $("[name = 'pengampu2']").val();
    $.ajax({
      type:'POST',
      data:'id_jdetail='+id_jdetail+'&hari2='+hari2+'&jam2='+jam2+'&pengampu2='+pengampu2,//'&itemMapel2='+itemMapel2+
      chace : false,
      url :'<?= base_url().'set_jadwal/ubahjadwaldetail'?>',
      dataType:'json',
        success:function(hasil){
          console.log(hasil);
          $('#pesan3').html(hasil.pesan);
          if (hasil.pesan=='') {
            tampilJadwalDetail();
            setTimeout(function () {
              $("[data-dismiss=modal]").trigger({
                type: "click"
              });
            },100)
            Swal.fire({
                title: 'Berhasil ',
                text: 'Jadwal Mata Pelajaran Telah Diubah!',
                type: 'success'
            });
          }
      }
    });
  }
  function tampilJadwalDetail(){
    $.ajax({
        type:'POST',
        url :'<?= base_url().'set_jadwal/tampiljadwaldetail'?>',
        dataType:'json',
          success:function(data){
            var baris ='';
            var n='';
            for(var i=0;i<data.length;i++){
              n=i+1;
              baris += '<tr>'+
                            '<td>'+  n +'</td>'+
                            '<td>'+ data[i].nama_kelas+'-'+ data[i].thn_ajaran+'</td>'+
                            '<td>'+ data[i].nama_mapel+'</td>'+
                            '<td>'+ data[i].nama_hari+'</td>'+
                            '<td>'+ data[i].jam+'</td>'+
                            '<td>'+ data[i].nama_guru+'</td>'+
                            '<td><a onclick="JadwalDetail('+ data[i].id_jadwal_detail+')" data-toggle="modal" data-target="#editjadwal" data-whatever="@getbootstrap" class="btn-circle btn-success btn-sm"><i class="fas fa-edit"></i></a>'+
                            '<a onclick="hapusjadwaldetail('+ data[i].id_jadwal_detail+')" class="btn-circle btn-danger btn-sm hapus"><i class="fas fa-trash"></i></a></td>'+
                       '</tr>';
            }
            $('#target').html(baris);
          }
      });
  }
  function ambilJadwal() {
    $.ajax({
      type:'POST',
      url :'<?= base_url().'set_jadwal/ambiljadwal'?>',
      dataType:'json',
      success:function(data){
          $('#jadwal').val(data['id_jadwal']);
      }
    });
  }
  function tambahJadwal(){
    var kelas = $("[name = 'kelas']").val();
    var nama_jadwal = $("[name = 'nama_jadwal']").val();
      $.ajax({
        type:'POST',
        data:'kelas='+kelas+'&nama_jadwal='+nama_jadwal,
        url :'<?= base_url().'set_jadwal/tambahjadwal'?>',
        chace : false,
        dataType:'json',
          success:function(hasil){
            $('#pesan').html(hasil.pesan);
            if (hasil.pesan=='') {
              document.getElementById('addJadwal').disabled=true;
              document.getElementById('kelas').disabled=true;
              document.getElementById('nama_jadwal').disabled=true;
              ambilJadwal();
              Swal.fire({
                  title: 'Berhasil ',
                  text: 'Jadwal Telah Disimpan!',
                  type: 'success'
              });
            }
          }
      });
  }

  function hapusjadwaldetail(id) {
    Swal.fire({
        title: "Apakah anda yakin?",
        text: "data akan dihapus!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya, Hapus Data!'
    }).then((result) => {
        if (result.value) {
          $.ajax({
            type:'POST',
            data:'id_jadwal_detail='+id,
            url:'<?= base_url().'set_jadwal/hapusjadwaldetail'?>',
            success : function() {
                tampilJadwalDetail();
            }
          });
        }
    });

  }



</script>
