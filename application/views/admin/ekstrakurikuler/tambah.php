<div class="success-data" data-success="<?= $this->session->flashdata('success'); ?>"></div>
<div class="warning-data" data-warning="<?= $this->session->flashdata('warning'); ?>"></div>
<div class="error-data" data-error="<?= $this->session->flashdata('error'); ?>"></div>

<div class="container-fluid">
  <!-- /.card-header -->
  <section class="content">
      <div class="card">
          <div class="card-header">
              <h3 class="card-title">Tambah Ekstrakurikuler</h3>
          </div>
            <div class="card-body table-responsive">
                <?php if (validation_errors()) : ?>
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <h5><i class="icon fas fa-ban"></i>Alert!</h5>
                        <?= validation_errors(); ?>
                    </div>
                <?php endif; ?>
                <?php echo form_open_multipart('ekstrakurikuler/tambah'); ?>
                  <div class="card-body col-lg-4">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Nama Ekstrakurikuler</label>
                        <input type="text" name="nama_eks" class="form-control">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">KKM</label>
                        <input type="number" name="kkm_eks" class="form-control">
                    </div>
                  <button type="submit" name="simpan" class="btn btn-primary">Simpan</button>
                </div>
          </div>
        </div>
      </section>
</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->
