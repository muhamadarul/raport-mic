<div class="success-data" data-success="<?= $this->session->flashdata('success'); ?>"></div>
<div class="warning-data" data-warning="<?= $this->session->flashdata('warning'); ?>"></div>
<div class="error-data" data-error="<?= $this->session->flashdata('error'); ?>"></div>

<div class="container-fluid">

  <!-- Page Heading -->
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Ekstrakurikuler &nbsp;<i class="fas fa-date"></i></h1>
      <a href="<?= base_url('ekstrakurikuler/cetak'); ?>" target="_blank" class="d-none d-sm-inline-block btn btn-md btn-danger shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report PDF</a>
  </div>
        <!-- Table -->
              <div class="card shadow mb-4">
                <div class="card-header py-3">
                  <h6></h6><a href="<?= base_url('ekstrakurikuler/tambah'); ?>" class="btn btn-primary btn-sm float-right"><i class="fa fa-plus">&nbsp; Tambah Data</i></a>
                </div>
                <div class="card-body">
                  <div class="table-responsive">
                    <table class="table table-bordered" id="example1">
                      <thead>
                        <tr>
                          <th>No</th>
                          <th>Ekstrakurikuler</th>
                          <th>KKM</th>
                          <th>Aksi</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php $no = 0; foreach($view as $row) : $no++ ?>
                        <tr>
                          <td><?= $no; ?></td>
                          <td><?= $row['nama_eks']; ?></td>
                          <td><?= $row['kkm_eks']; ?></td>
                          <td>
                            <a href="<?= base_url('ekstrakurikuler/ubah/'); ?><?= $row['id_eks']; ?>" class="btn-circle btn-success btn-sm"><i class="fas fa-edit"></i></a>
                            <?php if ($user['akses']==1) :?>
                            <a href="<?= base_url('ekstrakurikuler/hapus/'); ?><?= $row['id_eks']; ?>" class="btn-circle btn-danger btn-sm tombol-hapus"><i class="fas fa-trash"></i></a>
                            <?php endif;?>
                          </td>
                        </tr>
                      <?php endforeach; ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->
