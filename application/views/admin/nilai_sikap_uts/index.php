<div class="success-data" data-success="<?= $this->session->flashdata('success'); ?>"></div>
<div class="warning-data" data-warning="<?= $this->session->flashdata('warning'); ?>"></div>
<div class="error-data" data-error="<?= $this->session->flashdata('error'); ?>"></div>

<div class="container-fluid">

  <!-- Page Heading -->
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Nilai Sikap UTS&nbsp;<i class="fas fa-date"></i></h1>
  </div>
        <!-- Table -->
              <div class="card shadow mb-4">
                <div class="card-header py-3">
                  <h6></h6><a href="" data-toggle="modal" data-target="#tambah" data-whatever="@getbootstrap" class="btn btn-primary btn-sm float-right"><i class="fa fa-plus">&nbsp; Tambah Data</i></a>
                </div>
                <div class="card-body">
                  <div class="table-responsive">
                    <table class="table table-bordered" id="Mydata">
                      <thead>
                        <tr>
                          <th>No</th>
                          <th>Tahun Ajaran</th>
                          <th>Semester</th>
                          <th>Kelas</th>
                          <th>Nilai</th>
                          <?php if ($user['akses']==1):?>
                          <th>Aksi</th>
                          <?php endif;?>
                        </tr>
                      </thead>
                      <tbody id="target">
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->
<!-- MODAL TAMBAH   -->
      <div class="modal fade" id="tambah" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Tambah Nilai Sikap UTS</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <center><font color="red"><p id="pesan"></p></font></center>
              <form id="formEdit">
                <div class="form-group">
                    <label>Tahun Ajaran</label>
                    <select name="ta" id="ta" class="form-control" style="width: 100%;">
                      <option value="">-- Pilih Tahun Ajaran --</option>
                      <?php foreach ($tahun as $row): ?>
                          <option value="<?= $row['id_ta'];?>"><?= $row['thn_ajaran'];?></option>
                      <?php endforeach; ?>
                    </select>
                </div>
                <div class="form-group">
                    <label>Kelas</label>
                    <select name="kelas" id="kelas" class="form-control" style="width: 100%;">
                      <option value="">-- Pilih Kelas --</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>Semester</label>
                    <select name="semester" class="form-control" style="width: 100%;">
                      <option value="">-- Pilih Semester --</option>
                      <option value="1">Ganjil</option>
                      <option value="2">Genap</option>
                    </select>
                </div>
              </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="button" class="btn btn-primary" id="btnModal" onclick="tambah()" >Simpan</button>
            </div>
          </div>
        </div>
      </div>

      <!-- Large modal -->
      <form class="" method="post" id="update_form">
        <div class="modal fade nilai" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" data-backdrop="false" data-keyboard="false">
          <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Input Nilai Sikap UTS</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <div class="row">
                  <div class="col-md-4">
                    <div class="table-responsive">
                      <table>
                        <thead>
                          <tr>
                            <td>Tahun Ajaran</td>
                            <td>:</td>
                            <td id="v_thn_ajaran"></td>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>Kelas</td>
                            <td>:</td>
                            <td id="v_kelas"></td>
                          </tr>
                          <tr>
                            <td>Semester</td>
                            <td>:</td>
                            <td id="v_semester"></td>
                          </tr>
                          <tr>
                            <td>Wali Kelas</td>
                            <td>:</td>
                            <td id="v_guru"></td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <div class="table-responsive">
                      <table class="table table-bordered" id="Mydata">
                        <thead>
                          <tr>
                            <th>No</th>
                            <th>Nama Siswa</th>
                            <th>Nilai Spiritual</th>
                            <th>Predikat Spiritual</th>
                            <th>Nilai Sosial</th>
                            <th>Predikat Sosial</th>
                          </tr>
                        </thead>
                        <tbody id="tbl_nilai">
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
              <div class="modal-footer">
                <!-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> -->
                <button type="submit" class="btn btn-primary" id="btnModalNilai" >Simpan</button>
              </div>
            </div>
          </div>
        </div>
      </form>


<script type="text/javascript">
  $(document).ready(function() {
     view();
    $('#Mydata').dataTable();

      $('#ta').change(function() {
        var id_ta = $(this).val();
        $.ajax({
          type:'post',
          data:'id_ta='+id_ta,
          dataType:'json',
          url:'<?= base_url().'nilai_sikap_uts/ambilkelas'?>',
          success:function (data) {
              var html ='';
              var i;
              for (var i = 0; i < data.length; i++) {
                html += '<option value="'+data[i].id_kelas+'">'+data[i].nama_kelas+ " - " +data[i].thn_ajaran+'</option>';
              }
              $('#kelas').html(html);
          }
        });
      });
  });
  function view() {
    $.ajax({
      type:'ajax',
      url:'<?= base_url().'nilai_sikap_uts/view'?>',
      dataType:'json',
      async:false,
      success:function(data) {
        var baris ='';
        var n='';
        for(var i=0;i<data.length;i++){
          n=i+1;
          baris += '<tr>'+
                        '<td>'+  n +'</td>'+
                        '<td>'+ data[i].thn_ajaran+'</td>'+
                        '<td>'+ data[i].semester+'</td>'+
                        '<td>'+ data[i].nama_kelas+'-'+ data[i].thn_ajaran+'</td>'+
                        '<td><button type="button" onclick="inputNilai('+ data[i].id_sikap_uts+')" class="btn btn-success fa fa-edit" data-toggle="modal" data-target=".nilai">Input Nilai</button></td>'+
                        <?php if ($user['akses']==1):?>
                        '<td><button type="button" onclick="hapusNilai('+ data[i].id_sikap_uts+')" class="btn btn-danger hapus">Hapus</button></td>'+
                        <?php endif;?>
                   '</tr>';
        }
        $('#target').html(baris);
      }
    });
  }

  function tambah() {
    var ta = $("[name = 'ta']").val();
    var kelas = $("[name = 'kelas']").val();
    var semester = $("[name = 'semester']").val();
      $.ajax({
        type:'POST',
        data:'ta='+ta+'&kelas='+kelas+'&semester='+semester,
        chace : false,
        url :'<?= base_url().'nilai_sikap_uts/tambah'?>',
        dataType:'json',
          success:function(hasil){
            $('#pesan').html(hasil.pesan);
            if (hasil.pesan=='') {
              $.ajax({
                type:'POST',
                dataType:'json',
                url:'<?= base_url().'nilai_sikap_uts/ambilid'?>',
                success : function(data) {
                  var id = data.id_sikap_uts;
                  $.ajax({
                    type:'POST',
                    data:'id='+id+'&id_kelas='+kelas,
                    url:'<?= base_url().'nilai_sikap_uts/svd_nilaisikap'?>',
                    dataType:'json',
                    success:function () {
                        view();
                    }
                  });
                }
              });
              setTimeout(function () {
                $("[data-dismiss=modal]").trigger({
                  type: "click"
                });
              },100)
              Swal.fire({
                  title: 'Berhasil ',
                  text: 'Data berhasil disimpan!',
                  type: 'success'
              });
               view();
               document.getElementById('ta').value="";
               document.getElementById('kelas').value="";
            }
        }
      });
  }

  function inputNilai(id) {
    $.ajax({
      type:"POST",
      data:'id='+id,
      url:'<?= base_url().'nilai_sikap_uts/nilaisiswa'?>',
      dataType:'json',
      success:function(data) {
        var baris ='';
        var n='';
        for(var i=0;i<data.length;i++){
          n=i+1;
          baris += '<tr>'+
                        '<td>'+  n +'</td>'+
                        '<td>'+ data[i].nama_siswa+'</td>'+
                        '<td hidden><input name="id_nilai[]" type="text" value="'+ data[i].id_sikap_uts_detail+'" class="form-control" hidden></td>'+
                        '<td><textarea name="spritual[]" class="form-control">'+ data[i].spiritual+'</textarea></td>'+
                        '<td><input name="p_spiritual[]" type="text" value="'+ data[i].p_spiritual+'" class="form-control"></td>'+
                        '<td><textarea name="sosial[]" class="form-control">'+ data[i].sosial+'</textarea></td>'+
                        '<td><input name="p_sosial[]" type="text" value="'+ data[i].p_sosial+'" class="form-control" ></td>'+
                   '</tr>';
        }
        $('#tbl_nilai').html(baris);
        $.ajax({
          type:"POST",
          data:'id_sikap='+id,
          url:'<?= base_url().'nilai_sikap_uts/view_thnkelas'?>',
          dataType:'json',
          success:function (data) {
            $('#v_kelas').html(data.nama_kelas);
            $('#v_thn_ajaran').html(data.thn_ajaran);
            $('#v_guru').html(data.nama_guru);
            $('#v_semester').html(data.semester);
          }
        });
      }
    });

  }
  $('#update_form').on('submit', function(event){
    event.preventDefault();
        $.ajax({
            url:"<?= base_url().'nilai_sikap_uts/ubahnilai'?>",
            method:"POST",
            data:$(this).serialize(),
            success:function(data){
              Swal.fire({
                  title: 'Berhasil ',
                  text: 'Data berhasil disimpan!',
                  type: 'success'
              });
              setTimeout(function () {
                $("[data-dismiss=modal]").trigger({
                  type: "click"
                });
              },100)
            }
        })
  });
  function hapusNilai(id) {
    Swal.fire({
        title: "Apakah anda yakin?",
        text: "data akan dihapus!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya, Hapus Data!'
    }).then((result) => {
        if (result.value) {
          $.ajax({
            type:'POST',
            data:'id='+id,
            url:'<?= base_url().'nilai_sikap_uts/hapus'?>',
            success : function() {
              Swal.fire({
                  title: 'Berhasil ',
                  text: 'Data berhasil dihapus!',
                  type: 'success'
              });
                view();
            }
          });
        }
    });

  }

</script>
