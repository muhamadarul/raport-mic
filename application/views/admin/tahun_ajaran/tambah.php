<div class="success-data" data-success="<?= $this->session->flashdata('success'); ?>"></div>
<div class="warning-data" data-warning="<?= $this->session->flashdata('warning'); ?>"></div>
<div class="error-data" data-error="<?= $this->session->flashdata('error'); ?>"></div>

<div class="container-fluid">
  <!-- /.card-header -->
  <section class="content">
      <div class="card">
          <div class="card-header">
              <h3 class="card-title">Tambah Tahun Ajaran</h3>
          </div>
            <div class="card-body table-responsive">
                <?php if (validation_errors()) : ?>
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <h5><i class="icon fas fa-ban"></i>Alert!</h5>
                        <?= validation_errors(); ?>
                    </div>
                <?php endif; ?>
                <?php echo form_open_multipart('tahun_ajaran/tambah'); ?>
                  <div class="card-body col-lg-4">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Tahun Ajaran</label>
                        <input type="text" name="tahun_ajaran" class="form-control">
                    </div>
                  <button type="submit" name="simpan" class="btn btn-primary">Simpan</button>
                </div>
          </div>
        </div>
      </section>
</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->
