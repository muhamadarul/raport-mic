<div class="success-data" data-success="<?= $this->session->flashdata('success'); ?>"></div>
<div class="warning-data" data-warning="<?= $this->session->flashdata('warning'); ?>"></div>
<div class="error-data" data-error="<?= $this->session->flashdata('error'); ?>"></div>

<div class="container-fluid">

  <!-- /.card-header -->
  <section class="content">

      <div class="card">
          <div class="card-header">
              <h3 class="card-title">Edit Profil Sekolah</h3>
          </div>
                <div class="card-body table-responsive">
                    <?php if (validation_errors()) : ?>
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h5><i class="icon fas fa-ban"></i>Alert!</h5>
                            <?= validation_errors(); ?>
                        </div>
                    <?php endif; ?>
                    <?php echo form_open_multipart('profil_sekolah'); ?>
                      <div class="card-body">
                        <div class="form-group">
                            <label for="">Nama Sekolah</label>
                            <input name="nama_sekolah" type="text" class="form-control"  placeholder="Nama Sekolah" value="<?= $view['nama_sekolah'];?>" required>
                        </div>
                        <div class="form-group">
                            <label for="">Visi</label>
                            <textarea name="visi" class="textarea" placeholder="Tuliskan visi sekolah..."rows="8" cols="80"><?= $view['visi'];?></textarea>
                        </div>
                        <div class="form-group">
                            <label for="">Misi</label>
                            <textarea name="misi" class="textarea" placeholder="Tuliskan misi sekolah..." rows="8" cols="80"><?= $view['misi'];?></textarea>
                        </div>
                        <div class="form-group">
                          <label for="">Nama Kepala Sekolah</label>
                          <input type="text" name="kepsek" class="form-control" value="<?= $view['kepsek'];?>" required>
                        </div>
                        <div class="form-group">
                          <label for="">Email</label>
                          <input type="email" name="email" class="form-control" value="<?= $view['email'];?>">
                        </div>
                        <div class="form-group">
                          <label for="">Telepon</label>
                          <input type="text" name="telpon" class="form-control" value="<?= $view['telpon'];?>">
                        </div>
                        <div class="form-group">
                          <label for="">Website</label>
                          <input type="text" name="web" class="form-control" value="<?= $view['web'];?>">
                        </div>
                        <div class="form-group">
                          <label for="">Alamat</label>
                          <textarea name="alamat" class="form-control" rows="8" cols="80"><?= $view['alamat'];?></textarea>
                        </div>
                        <div class="form-group">
                          <label for="">Logo</label><strong><span>| Isi jika ingin ubah logo sekolah</span></strong><br>
                          <input type="file" name="logo" value="">
                        </div>
                        <input type="text" name="logo_lama" value="<?= $view['logo'];?>" hidden>
                        <input name="id" type="text" value="<?= $view['id'];?>" hidden>
                      <button type="submit" name="simpan" class="btn btn-primary">Simpan</button>
                    </div>
              </div>
        </div>
      </section>
    </div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->
