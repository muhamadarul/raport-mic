<div class="success-data" data-success="<?= $this->session->flashdata('success'); ?>"></div>
<div class="warning-data" data-warning="<?= $this->session->flashdata('warning'); ?>"></div>
<div class="error-data" data-error="<?= $this->session->flashdata('error'); ?>"></div>

<div class="container-fluid">
  <?php
  $id_jadwal= $this->uri->segment(3);
   ?>
  <!-- Page Heading -->
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Detail Jadwal &nbsp;<i class="fas fa-pen"></i></h1>
    <a href="<?= base_url('jadwal/cetak/'); ?><?= $id_jadwal; ?>" target="_blank" class="d-none d-sm-inline-block btn btn-md btn-danger shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report PDF</a>
  </div>

                <section class="content">
                  <div class="row">
                    <div class="col-md-6">
                      <div class="card">
                        <ul class="list-group list-group-flush">
                          <li class="list-group-item">Nama Jadwal       : &nbsp; <?= $view["nama_jadwal"]; ?> </li>
                          <li class="list-group-item">Tahun Ajaran       : &nbsp; <?= $view["thn_ajaran"]; ?> </li>
                          <li class="list-group-item">Kelas       : &nbsp; <?= $view["nama_kelas"]; ?> </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </section>
                    <br>
                    <!-- Table -->
                  <div class="card shadow mb-4">
                    <div class="row">
                      <div class="col-md-12">
                        <div class="card-body">
                          <div class="table-responsive">
                            <table class="table table-bordered" id="example1">
                              <thead>
                                <tr>
                                  <th>NO</th>
                                  <th>Mapel</th>
                                  <th>Hari</th>
                                  <th>Jam</th>
                                  <th>Pengampu</th>
                                </tr>
                              </thead>
                              <tbody>
                              <?php $no = 0; foreach($jadwal as $row) : $no++ ?>
                                <tr>
                                  <td><?= $no; ?></td>
                                  <td><?= $row['nama_mapel']; ?></td>
                                  <td><?= $row['nama_hari']; ?></td>
                                  <td><?= $row['jam']; ?></td>
                                  <td><?= $row['nama_guru']; ?></td>
                                </tr>
                              <?php endforeach; ?>
                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                    <a  href="<?= base_url('jadwal'); ?>" class="btn btn-primary btn-sm float-left"><i class="fa fa-sign-out-alt">&nbsp; Kembali</i></a>

                </div>




<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->
