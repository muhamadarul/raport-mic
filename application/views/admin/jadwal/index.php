<div class="success-data" data-success="<?= $this->session->flashdata('success'); ?>"></div>
<div class="warning-data" data-warning="<?= $this->session->flashdata('warning'); ?>"></div>
<div class="error-data" data-error="<?= $this->session->flashdata('error'); ?>"></div>

<div class="container-fluid">

  <!-- Page Heading -->
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Jadwal &nbsp;<i class="fas fa-date"></i></h1>
  </div>

        <!-- Table -->
              <div class="card shadow mb-4">
                <div class="card-header py-3">
                </div>
                <div class="card-body">
                  <div class="table-responsive">
                    <table class="table table-bordered" id="example1">
                      <thead>
                        <tr>
                          <th>No</th>
                          <th>Nama Jadwal</th>
                          <th>Kelas</th>
                          <th>Aksi</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php $no = 0; foreach($view as $row) : $no++ ?>
                        <tr>
                          <td><?= $no; ?></td>
                          <td><?= $row['nama_jadwal']; ?></td>
                          <td><?= $row['nama_kelas']; ?>&nbsp; <?= $row['thn_ajaran']?></td>
                          <td>
                            <a href="<?= base_url('jadwal/ubah/'); ?><?= $row['id_jadwal']; ?>" class="btn-circle btn-success btn-sm"><i class="fas fa-edit"></i></a>
                            <a href="<?= base_url('jadwal/detail/'); ?><?= $row['id_jadwal']; ?>" class="btn-circle btn-warning btn-sm"><i class="fas fa-eye"></i></a>
                            <?php if ($user['akses']==1) :?>
                            <a href="<?= base_url('jadwal/hapus/'); ?><?= $row['id_jadwal']; ?>" class="btn-circle btn-danger btn-sm tombol-hapus"><i class="fas fa-trash"></i></a>
                          <?php endif;?>
                          </td>
                        </tr>
                      <?php endforeach; ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->
