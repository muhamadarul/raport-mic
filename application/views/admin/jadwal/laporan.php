<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
    <style>
      table{
        border-collapse: collapse;
      }
      table, th, td{
        border: solid black;
      }
      th, td{
        padding: 10px;
      }
      th{
        background-color: rgb(19,110,170);
        color: white;
      }
      tr:hover{
        background-color: #f5f5f5;
      }
    </style>
  </head>
  <body>
    <center><h1><?= $sekolah['nama_sekolah'];?></h1></center>
    <center><p><?= $sekolah['alamat'];?></p></center>
    <center><p>Email : <?= $sekolah['email'];?>&nbsp; Website : <?= $sekolah['web'];?>&nbsp; Telepon : <?= $sekolah['telpon'];?></p></center>
    <hr/>
    <h3><center>Jadwal Pelajaran </center></h3>
    <div class="table-responsive">
    	<table class="table table-bordered" border=""  width="50%" style="text-align:left;">
        <tr>
          <td>Nama Jadwal</td>
          <td>:</td>
          <td><?= $view["nama_jadwal"]; ?></td>
        </tr>
    	<tr>
    		<td>Tahun Ajaran</td>
        <td>:</td>
        <td><?= $view["thn_ajaran"]; ?></td>
      </tr>
      <tr>
        <td>Nama Kelas</td>
        <td>:</td>
        <td><?= $view["nama_kelas"]; ?></td>
      </tr>
    </table>
    </div>

    <!-- Table -->
      <table border="1px" width="100%" style="text-align:center;">
          <thead>
            <tr>
              <th>NO</th>
              <th>Mapel</th>
              <th>Hari</th>
              <th>Jam</th>
              <th>Pengampu</th>
            </tr>
          </thead>
          <tbody>
          <?php $no = 0; foreach($jadwal as $row) : $no++ ?>
            <tr>
              <td><?= $no; ?></td>
              <td><?= $row['nama_mapel']; ?></td>
              <td><?= $row['nama_hari']; ?></td>
              <td><?= $row['jam']; ?></td>
              <td><?= $row['nama_guru']; ?></td>
            </tr>
          <?php endforeach; ?>
          </tbody>
        </table>
  </body>
</html>
